import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/MyData.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
class Healthy{
  String text;
  int index;
  Healthy({this.text, this.index});
}
class Edithealthy extends StatefulWidget {
  @override
  _EdithealthyState createState() => _EdithealthyState();
}
class _EdithealthyState extends State<Edithealthy> {
  int _currentIndex = 1;

  List<Healthy> _Healthy =[
    Healthy(
      text: "نعم",
      index: 1,
    ),
    Healthy(
      text: "لا",
      index: 2,
    ),
  ];
  @override
  Widget build(BuildContext context) {

    Color color=Color.fromARGB(242,12,82,123);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        bottomOpacity: 0.0,
        toolbarOpacity: 0.0,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.only(top: 5.0),
            icon: Image(image: AssetImage('img/backblack_@2x.png'),height: 40.0,width:30.0 ,),
            onPressed: ()=>Navigator.pop(context),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(top: 5.0,right: 10.0),
              child:Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('تعديل بيانات الصحة',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),textScaleFactor: 2,),
                   Row(
                     //mainAxisAlignment: MainAxisAlignment.end,
                     textDirection: TextDirection.rtl,
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: <Widget>[
                     Container(
                       margin: EdgeInsets.only(top: 38.0,right: 20.0,left: 20.0),
                       height: 130.0,
                         child:Column(
                           mainAxisSize: MainAxisSize.max,
                         //crossAxisAlignment: CrossAxisAlignment.end,
                         children: <Widget>[
                           Expanded(
                             flex: 5,
                               child:Text('الطول', style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,))),
                         Expanded(
                           flex: 5,
                           child:  Text(' الوزن', style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,)))
                         ],
                       )),
                       Container(
                           margin: EdgeInsets.only(top: 18.0,right: 20.0),
                           height: 130.0,
                           child:Column(
                            // mainAxisSize: MainAxisSize.max,
                             //crossAxisAlignment: CrossAxisAlignment.end,
                             children: <Widget>[
                               Expanded(
                                   flex: 5,
                                  child:Container(
                                   width: 220,
                                  child: Directionality(
                                    textDirection: TextDirection.rtl,
                                      child: TextFormField(
                                      //controller: name,
                                      decoration:InputDecoration(
                                        hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14 ),
                                        hintText: '175',
                                        enabledBorder:UnderlineInputBorder(
                                          borderSide:BorderSide(color:color,style: BorderStyle.solid),
                                        ),
                                        focusedBorder:UnderlineInputBorder(
                                          borderSide:BorderSide(color:color, ),
                                        ),
                                      )
                                  )))),
                               Expanded(
                                   flex: 5,
                                   child:Container(
                                       width: 220.0,
                                       child:Directionality(
                                           textDirection: TextDirection.rtl,
                                           child: TextFormField(
                                         //controller: name,
                                           decoration:InputDecoration(
                                             hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14, ),
                                             hintText: '75',
                                             enabledBorder:UnderlineInputBorder(
                                               borderSide:BorderSide(color:color,style: BorderStyle.solid),
                                             ),
                                             focusedBorder:UnderlineInputBorder(
                                               borderSide:BorderSide(color:color),
                                             ),
                                           )
                                       )))),
                             ],
                           ),),
                       Container(
                           margin: EdgeInsets.only(top: 43.0,right: 20.0,left: 10.0),
                           height: 130.0,
                           child:Column(
                             mainAxisSize: MainAxisSize.max,
                             //crossAxisAlignment: CrossAxisAlignment.end,
                             children: <Widget>[
                               Expanded(
                                   flex: 5,
                                   child:Text('سم', style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,))),
                               Expanded(
                                   flex: 5,
                                   child:  Text(' كجم', style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,)))
                             ],
                           )),
                     ],
                   ),
                  Padding(padding:EdgeInsets.symmetric(vertical: 10.0,horizontal: 35.0) ,
                    child: Text('هل تعاني من أي مشاكل صحية  ؟',
                        style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:15 )),
                  ),
                    Directionality(
                      textDirection: TextDirection.rtl,
                        child:Column(
                      //crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      textDirection: TextDirection.rtl,
                        children: _Healthy
                            .map((item) => RadioListTile(
                          selected: true,
                          activeColor: Colors.lightGreen,
                          groupValue: _currentIndex,
                          title: Text("${item.text}",style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w800),),
                          value: item.index,
                          onChanged: (val) {
                            setState(() {
                              _currentIndex = val;
                            });
                          },
                        )).toList()
                    ))
          ])),
          InkWell(
            onTap: ()=>Navigator.pop(context),
            child: Container(
              height: 40.0,
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.symmetric(vertical: 5.0),
              margin: EdgeInsets.symmetric(vertical: 30.0,horizontal: 100),
              decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(20.0)
              ),
              child: Text('حفظ',style: TextStyle(color: Colors.white,fontFamily: 'Cairo',),
              ),
            ),
          )

        ],
      ),
    );
  }
}
