import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Naotification2.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/pages/Home.dart';
import 'package:nepali_date_picker/nepali_date_picker.dart';
class NotificationPage extends StatefulWidget {
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  Color color=Color.fromARGB(242,12,82,123);
  bool viewVisible = true;
   hideWidget(){
    setState(() {
      viewVisible = false ;
    });
  }
  List numbers=[
    1,2,3
  ];
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child:Scaffold(
          backgroundColor: Colors.white,
          body: ListView(
            padding: EdgeInsets.only(top: 60.0,right: 5),
            children: <Widget>[
              Text('الإشعارات',textScaleFactor: 2,
                style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 17.0),),
             Container(
               height:MediaQuery.of(context).size.height*1,
               child: InkWell(
                 onTap:()=> Navigator.push(context,MaterialPageRoute(builder:(context)=>notification())),
                 child: ListView.builder(
                 padding: EdgeInsets.only(right:5.0,top: 15.0 ),
                   itemCount:numbers.length,
                   itemBuilder:(context,index){

                     return
                       Slidable(
                         delegate:  SlidableDrawerDelegate(),
                         child: Container(
                     child: ListTile(
                             title:  Row(
                               children: <Widget>[
                                 Text('مركز النصر الرياضي',
                                   style: TextStyle(color: Colors.black87,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:20 ),),
                                 Padding(
                                     padding:
                                     EdgeInsets.only(right: 5.0, top: 5.0),
                                     child: Row(
                                       children: <Widget>[
                                         Image(
                                           image: AssetImage(
                                               'img/baseline-star-24px.png'),
                                         ),
                                         Image(
                                           image: AssetImage(
                                               'img/baseline-star-24px.png'),
                                         ),
                                         Image(
                                           image: AssetImage(
                                               'img/baseline-star-24px.png'),
                                         ),
                                         Image(
                                           image: AssetImage(
                                               'img/baseline-star-24px.png'),
                                         ),
                                         Image(
                                           image: AssetImage('img/Path 1191.png'),
                                         ),
                                       ],
                                     )),
                               ],
                             ),
                             subtitle: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: <Widget>[
                                 Container(
                                   margin: EdgeInsets.only(top: 15.0),
                                   child: Text('تم تأكيد إشتراك مدة 3 شهور في مركز النصر الرياضي',
                                     style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500 ,),),
                                 ),
                                 Container(
                                   margin: EdgeInsets.only(top: 0.0),
                                   child: Text('2019/8/12',textDirection: TextDirection.rtl,
                                     style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500 ,),),
                                 ),
                                 Divider(color: Colors.grey,)
                               ],
                             ),
                           ),

                         ),
                         actions: <Widget>[
                    IconButton(icon: IconButton(icon: Image(image: AssetImage('img/Group 1196.png')), onPressed:(){
                      setState(() {
                        numbers.removeAt(index);
                      });
                    },)),
                       ],
                     );
                   }
               )),

             )
            ],
          ),
        ),
    );
  }
}
