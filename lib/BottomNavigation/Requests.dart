import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Notification.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/pages/Home.dart';

class Requests extends StatefulWidget {
  @override
  _RequestsState createState() => _RequestsState();
}

class _RequestsState extends State<Requests> {
  Color color = Color.fromARGB(242, 12, 82, 123);
 // int _currentIndex = 0;
  Timer _timer;
  int seconds;
  String constructTime(int seconds) {
    int hour = seconds ~/ 3600;
    int minute = seconds % 3600 ~/ 60;
    int second = seconds % 60;
    return formatTime(hour) +
        ":" +
        formatTime(minute) +
        ":" +
        formatTime(second);
  }

  String formatTime(int timeNum) {
    return timeNum < 10 ? "0" + timeNum.toString() : timeNum.toString();
  }

  @override
  void initState() {
    super.initState();
    var now = DateTime.now();
    var twoHours = now.add(Duration(minutes: 120)).difference(now);
    seconds = twoHours.inSeconds;
    startTimer();
  }

  void startTimer() {
    const period = const Duration(seconds: 1);
    _timer = Timer.periodic(period, (timer) {
      setState(() {
        seconds--;
      });
      if (seconds == 0) {
        cancelTimer();
      }
    });
  }

  void cancelTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
  }

  @override
  void dispose() {
    super.dispose();
    cancelTimer();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new DefaultTabController(
        initialIndex: 1,
        length: 2,
        child: Scaffold(
          //==================================Bottom Navigation Bar========================================
          /*bottomNavigationBar: Directionality(
        textDirection: TextDirection.rtl,
        child: BottomNavigationBar(
            //currentIndex: _currentIndex,
            backgroundColor: color,
            elevation: 0.0,
            unselectedItemColor: Colors.white30,
            selectedItemColor: Colors.lightGreen,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                  backgroundColor:color,
                  icon:IconButton(icon:Image(image: AssetImage('img/hometaplight@2x.png',),width: 30.0,),
                    onPressed:()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>Home())),
                  ),
                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon:IconButton(icon: Image(image: AssetImage('img/calendertap.png',),),
                    onPressed: ()=>Navigator.pop(context)),
                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon:IconButton(icon: Image(image: AssetImage('img/notificationstap.png',),),
                      onPressed:()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>NotificationPage()))) ,
                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon:IconButton(icon: Image(image: AssetImage('img/favouritetap.png',),),
                    onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>Favourit())),),
                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon:IconButton(icon: Image(image: AssetImage('img/profiletap.png',),),
                      onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>PersonalData()))),
                  title: Text('')
              ),
            ],
          )),*/
//=========================================================================================================
          backgroundColor: Colors.white,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: Container(
              margin: EdgeInsets.only(top: 10.0),
              height: 100.0,
              color: Colors.white,
              child: SafeArea(
                child: TabBar(
                  unselectedLabelColor: color,
                  labelColor: color,
                  indicatorColor: Colors.lightGreen,
                  tabs: [
                    Text('الإشتراكات',
                        style: TextStyle(
                            color: color,
                            fontFamily: 'Cairo',
                            fontWeight: FontWeight.w600)),
                    Text("الطلبات",
                        style: TextStyle(
                            color: color,
                            fontFamily: 'Cairo',
                            fontWeight: FontWeight.w600))
                  ],
                ),
              ),
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              //================================الاشتراكات=============================================
              ListView.builder(
                  itemCount: 1,
                  itemBuilder: (context, index) {
                    return Container(
                      color: Colors.white,
                      margin: EdgeInsets.only(top: 15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        //textDirection: TextDirection.rtl,
                        children: <Widget>[
                          Row(
                            //crossAxisAlignment: CrossAxisAlignment.end,
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Padding(
                                  padding:
                                      EdgeInsets.only(right: 25.0, top: 5.0),
                                  child: Text(
                                    'رقم الطلب',
                                    style: TextStyle(
                                        color: color,
                                        fontFamily: 'Cairo',
                                        fontWeight: FontWeight.w500),
                                  )),
                              Padding(
                                  padding:
                                      EdgeInsets.only(right: 10.0, top: 5.0),
                                  child: Text(
                                    '44333678509',
                                    style: TextStyle(
                                        color: color,
                                        fontFamily: 'Cairo',
                                        fontWeight: FontWeight.w500),
                                  ))
                            ],
                          ),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Padding(
                                  padding:
                                      EdgeInsets.only(right: 35.0, top: 0.0),
                                  child: Text(
                                    'مركز النصر الرياضي',
                                    style: TextStyle(
                                        color: color,
                                        fontFamily: 'Cairo',
                                        fontWeight: FontWeight.w800,
                                        fontSize: 20.0),
                                  )),
                              Padding(
                                  padding:
                                      EdgeInsets.only(right: 5.0, top: 5.0),
                                  child: Row(
                                    children: <Widget>[
                                      Image(
                                        image: AssetImage('img/Path 1191.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Text(
                                'تاريخ إرسال الطلب 2019/08/12',
                                style: TextStyle(
                                    color: color,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.w500),
                                textDirection: TextDirection.rtl,
                              )),
                          Padding(
                              padding: EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Text(
                                'تاريخ بداية الإشتراك 2019/08/21',
                                style: TextStyle(
                                    color: color,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.w500),
                                textDirection: TextDirection.rtl,
                              )),
                          Padding(
                              padding: EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Text(
                                'تاريخ نهاية الإشتراك 2019/09/21',
                                style: TextStyle(
                                    color: color,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.w500),
                                textDirection: TextDirection.rtl,
                              )),
                          Divider(
                            color: Colors.grey[700],
                          )
                        ],
                      ),
                    );
                  }),
              //===============================الطلبات======================================================
              ListView.builder(
                  itemCount: 2,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(top: 15.0),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        //textDirection: TextDirection.rtl,
                        children: <Widget>[
                          Container(
                              // height: 100.0,
                              width: MediaQuery.of(context).size.width * 1,
                              child: ListTile(
                                  leading: Container(
                                    margin: EdgeInsets.only(top: 20.0),
                                    width: 50.0,
                                    //height: 50.0,
                                    alignment: Alignment.topLeft,
                                    //======================================Bottom Sheet**************************************************************************
                                    child: IconButton(
                                      icon: Image(
                                          image:
                                              AssetImage('img/Group 1617.png')),
                                      onPressed: () {
                                        showModalBottomSheet(
                                            context: context,
                                            builder: (BuildContext bc) {
                                              return Container(
                                                margin:
                                                    EdgeInsets.only(top: 30.0),
                                                alignment: Alignment.center,
                                                height: 120.0,
                                                child: Column(
                                                  children: <Widget>[
                                                    InkWell(
                                                      onTap: () {
                                                        showDialog(
                                                            context: context,
                                                            builder:
                                                                (BuildContext
                                                                    context) {
                                                              return AlertDialog(
                                                                shape: RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            20.0)),
                                                                title:
                                                                    Container(
                                                                        height:
                                                                            250,
                                                                        width:
                                                                            340,
                                                                        child: Column(
                                                                            children: <Widget>[
                                                                              Padding(
                                                                                  padding: EdgeInsets.only(top: 10.0),
                                                                                  child: Text(
                                                                                    'تستطيع تعديل الطلب خلال ساعتين',
                                                                                    style: TextStyle(color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w600, fontSize: 15),
                                                                                  )),
                                                                              Text(
                                                                                'بعد إجراء الطلب',
                                                                                style: TextStyle(color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w600, fontSize: 15),
                                                                              ),
                                                                              Padding(
                                                                                  padding: EdgeInsets.only(top: 10.0),
                                                                                  child: Text(
                                                                                    "" + constructTime(seconds) + '',
                                                                                    style: TextStyle(color: Colors.lightGreen, fontSize: 20, letterSpacing: 3, wordSpacing: 3),
                                                                                  )),
                                                                              Padding(
                                                                                  padding: EdgeInsets.only(top: 10.0),
                                                                                  child: Text(
                                                                                    'هل تريد تاكيد تعديل تاريخ الطلب',
                                                                                    style: TextStyle(
                                                                                      fontFamily: 'Cairo',
                                                                                      fontWeight: FontWeight.w500,
                                                                                    ),
                                                                                  )),
                                                                              Row(
                                                                                textDirection: TextDirection.rtl,
                                                                                children: <Widget>[
                                                                                  InkWell(
                                                                                    onTap: () => Navigator.pop(context),
                                                                                    child: Container(
                                                                                      alignment: Alignment.center,
                                                                                      margin: EdgeInsets.only(top: 30.0, right: 20.0),
                                                                                      width: 130.0,
                                                                                      height: 40.0,
                                                                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: color, width: 1.5)),
                                                                                      child: Text(
                                                                                        'تجاهل',
                                                                                        style: TextStyle(
                                                                                          color: color,
                                                                                          fontFamily: 'Cairo',
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                  Expanded(
                                                                                      child: InkWell(
                                                                                    onTap: () {},
                                                                                    child: Container(
                                                                                      alignment: Alignment.center,
                                                                                      margin: EdgeInsets.only(top: 30.0, right: 20.0),
                                                                                      //width: 130.0,
                                                                                      height: 40.0,
                                                                                      decoration: BoxDecoration(
                                                                                        color: color,
                                                                                        borderRadius: BorderRadius.circular(10),
                                                                                      ),
                                                                                      child: Text(
                                                                                        'تأكيد',
                                                                                        style: TextStyle(
                                                                                          color: Colors.white,
                                                                                          fontFamily: 'Cairo',
                                                                                          fontWeight: FontWeight.w500,
                                                                                        ),
                                                                                        textAlign: TextAlign.center,
                                                                                      ),
                                                                                    ),
                                                                                  )),
                                                                                ],
                                                                              )
                                                                            ])),
                                                              );
                                                            });
                                                      },
                                                      child: Text('تعديل تاريخ البداية',
                                                        style: TextStyle(
                                                            color: Colors.grey[600],
                                                            fontFamily: 'Cairo',
                                                            fontWeight:
                                                                FontWeight.w600,
                                                            fontSize: 17),
                                                      ),
                                                    ),
                                                    Divider(
                                                      color: Colors.grey[600],
                                                      height: 30,
                                                    ),
                                                    //============================================================== تاريخ الغاء طلب *****************************************==================
                                                    Container(
                                                        margin: EdgeInsets.only(
                                                            top: 5.0),
                                                        alignment:
                                                            Alignment.center,
                                                        child: InkWell(
                                                          onTap: () {
                                                            showDialog(
                                                                context:
                                                                    context,
                                                                builder:
                                                                    (BuildContext
                                                                        context) {
                                                                  return AlertDialog(
                                                                    shape: RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(20.0)),
                                                                    title: Container(
                                                                        height: 250,
                                                                        width: 340,
                                                                        child: Column(children: <Widget>[
                                                                          Padding(
                                                                              padding: EdgeInsets.only(top: 10.0),
                                                                              child: Text(
                                                                                'تستطيع إلغاء الطلب خلال ساعتين',
                                                                                style: TextStyle(color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w600, fontSize: 15),
                                                                              )),
                                                                          Text(
                                                                            'بعد إجراء الطلب',
                                                                            style: TextStyle(
                                                                                color: color,
                                                                                fontFamily: 'Cairo',
                                                                                fontWeight: FontWeight.w600,
                                                                                fontSize: 15),
                                                                          ),
                                                                          Padding(
                                                                              padding: EdgeInsets.only(top: 10.0),
                                                                              child: Text(
                                                                                "" + constructTime(seconds) + '',
                                                                                style: TextStyle(color: Colors.lightGreen, fontSize: 20, letterSpacing: 3, wordSpacing: 3),
                                                                              )),
                                                                          Padding(
                                                                              padding: EdgeInsets.only(top: 10.0),
                                                                              child: Text(
                                                                                'هل تريد تاكيد إلغاء الطلب',
                                                                                style: TextStyle(
                                                                                  fontFamily: 'Cairo',
                                                                                  fontWeight: FontWeight.w500,
                                                                                ),
                                                                              )),
                                                                          InkWell(
                                                                              onTap: (){},
                                                                              child: Container(
                                                                                margin: EdgeInsets.symmetric(vertical: 20),
                                                                                child: Row(
                                                                                  textDirection: TextDirection.rtl,
                                                                                  children: <Widget>[
                                                                                    Expanded(
                                                                                      child: InkWell(
                                                                                        onTap: () => Navigator.pop(context),
                                                                                        child: Container(
                                                                                          alignment: Alignment.center,
//                                                                                      width: 130.0,
                                                                                          height: 40.0,
                                                                                          decoration: BoxDecoration(
                                                                                            borderRadius: BorderRadius.circular(10),
                                                                                            border: Border.all(color: color, width: 1.5),
                                                                                          ),
                                                                                          child: Text(
                                                                                            'تجاهل',
                                                                                            style: TextStyle(
                                                                                              color: color,
                                                                                              fontFamily: 'Cairo',
                                                                                              fontWeight: FontWeight.w500,
                                                                                            ),
                                                                                            textAlign: TextAlign.center,
                                                                                          ),
                                                                                        ),
                                                                                      ),
                                                                                    ),
                                                                                    SizedBox(width: 10,),
                                                                                    Expanded(
                                                                                        child: InkWell(
                                                                                      onTap: ()=>Navigator.pop(context),
                                                                                      child: Container(
                                                                                        alignment: Alignment.center,
                                                                                        //   margin: EdgeInsets.only(top: 30.0, right: 20.0),
                                                                                        //width: 130.0,
                                                                                        height: 40.0,
                                                                                        decoration: BoxDecoration(
                                                                                          color: Colors.red[800],
                                                                                          borderRadius: BorderRadius.circular(10),
                                                                                        ),
                                                                                        child: Text(
                                                                                          'تأكيد',
                                                                                          style: TextStyle(
                                                                                            color: Colors.white,
                                                                                            fontFamily: 'Cairo',
                                                                                            fontWeight: FontWeight.w500,
                                                                                          ),
                                                                                          textAlign: TextAlign.center,
                                                                                        ),
                                                                                      ),
                                                                                    )),
                                                                                  ],
                                                                                ),
                                                                              )),
                                                                        ])),
                                                                  );
                                                                });
                                                          },
                                                          child: Text(
                                                            'إلغاء الطلب',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .grey[600],
                                                                fontFamily:
                                                                    'Cairo',
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                fontSize: 17),
                                                          ),
                                                        )),
                                                  ],
                                                ),
                                              );
                                            });
                                      },
                                    ),
                                  ),
                                  title: Row(
                                    //crossAxisAlignment: CrossAxisAlignment.end,
                                    textDirection: TextDirection.rtl,
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.only(right: 5.0),
                                          child: Text(
                                            'رقم الطلب',
                                            style: TextStyle(
                                                color: color,
                                                fontFamily: 'Cairo',
                                                fontWeight: FontWeight.w500),
                                          )),
                                      Padding(
                                          padding: EdgeInsets.only(right: 10.0),
                                          child: Text(
                                            '44333678509',
                                            style: TextStyle(
                                                color: color,
                                                fontFamily: 'Cairo',
                                                fontWeight: FontWeight.w500),
                                          ))
                                    ],
                                  ))),
                          Row(
                            textDirection: TextDirection.rtl,
                            children: <Widget>[
                              Padding(
                                  padding:
                                      EdgeInsets.only(right: 35.0, top: 0.0),
                                  child: Text(
                                    'مركز النصر الرياضي',
                                    style: TextStyle(
                                        color: color,
                                        fontFamily: 'Cairo',
                                        fontWeight: FontWeight.w800,
                                        fontSize: 20.0),
                                  )),
                              Padding(
                                  padding:
                                      EdgeInsets.only(right: 5.0, top: 5.0),
                                  child: Row(
                                    children: <Widget>[
                                      Image(
                                        image: AssetImage('img/Path 1191.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Text(
                                'تاريخ إرسال الطلب 2019/08/12',
                                style: TextStyle(
                                    color: color,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.w500),
                                textDirection: TextDirection.rtl,
                              )),
                          Padding(
                              padding: EdgeInsets.only(right: 20.0, top: 5.0),
                              child: Text(
                                'تاريخ بداية الإشتراك 2019/08/21',
                                style: TextStyle(
                                    color: color,
                                    fontFamily: 'Cairo',
                                    fontWeight: FontWeight.w500),
                                textDirection: TextDirection.rtl,
                              )),
                          Divider(
                            color: Colors.grey[700],
                          )
                        ],
                      ),
                    );
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
