import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
class EditLang extends StatefulWidget {
  @override
  _EditLangState createState() => _EditLangState();
}

class _EditLangState extends State<EditLang> {
  Color color=Color.fromARGB(242,12,82,123);
  bool _isVisible = false;
   int _currentindex=0;
  void showIcon() {
    setState(() {
      _isVisible =!_isVisible;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        bottomOpacity: 0.0,
        toolbarOpacity: 0.0,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.only(top: 5.0),
            icon: Image(image: AssetImage('img/backblack_@2x.png'),height: 40.0,width:30.0 ,),
            onPressed: ()=>Navigator.pop(context),
          ),
        ],
      ),
      body:SingleChildScrollView(
        child:  Container(
            height: MediaQuery.of(context).size.height*1,
            alignment: Alignment.topRight,
            margin: EdgeInsets.only(top:10.0,right: 15.0),
            child:Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text('اللغة',
                    style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),textScaleFactor: 2,),
                  InkWell(
                    onTap: showIcon,
                      child:Container(
                    margin: EdgeInsets.only(top: 10.0,right: 0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      textDirection: TextDirection.rtl,
                      children: <Widget>[
                        Text('العربية', style: TextStyle(color:Colors.grey[600],fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18)),
                        Spacer(),
                        Visibility(
                          visible: _isVisible,
                          replacement:Padding(padding:EdgeInsets.only(left: 15.0) ,
                              child: Image.asset('img/checkedbox.png')) ,
                          child: Container(),),
                      ],
                    ),
                  )),
                  Divider(color: Colors.grey,),
                  InkWell(
                    onTap: showIcon,
                      child:Container(
                    margin: EdgeInsets.only(top: 10.0,right: 0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      textDirection: TextDirection.rtl,
                      children: <Widget>[
                        Text('English', style: TextStyle(color:Colors.grey[600],fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18)),
                        Spacer(),
                        Visibility(
                            visible: _isVisible,
                            child:Padding(padding:EdgeInsets.only(left: 15.0) ,
                                child:_isVisible? Image.asset('img/checkedbox.png'):Text('kkkk'))),
                       // Spacer(),

                      ],
                    ),
                  )),
                  Divider(color: Colors.grey,),

        ])
      ),
      )

    );
  }
}

/*Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                       Text('العربية', style: TextStyle(color:Colors.grey[600],fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18)),
                        Spacer(),

                      ],
                    ),*/
/*
Visibility(
                            visible: _isVisible,
                            replacement:Padding(padding:EdgeInsets.only(left: 15.0) ,
                                child: Image.asset('img/checkedbox.png')) ,
                            child: Container(),),
 */