import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Favourit2.dart';
import 'package:healthyzone/BottomNavigation/Naotification2.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/pages/Centers.dart';
import 'package:healthyzone/pages/Home.dart';
class Favourit extends StatefulWidget {
  @override
  _FavouritState createState() => _FavouritState();
}

class _FavouritState extends State<Favourit> {
  int _currentIndex = 0;
  Color color=Color.fromARGB(242,12,82,123);
  bool _isVisible = true;
  void showBtn() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }
  List numbers=[
    1,2,3
  ];

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
      backgroundColor: Colors.white,
      body:ListView(
       // padding: EdgeInsets.only(top: 30.0,right: 10,left: 10.0),
        children: <Widget>[
      Padding(padding: EdgeInsets.only(top: 30.0,right: 10,left: 10.0),child:  Text('المفضله',textScaleFactor: 2,
          style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 17.0),)),
        Container(
          height:500.0,
            width: MediaQuery.of(context).size.width*1,
            child: ListView.builder(
           itemCount: numbers.length,
             itemBuilder: (context,index){
           return  Card(
             elevation: 5.0,
             shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.circular(10.0),
             ),
             child:InkWell(
                 onTap:   ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Favouritpage())),
                 child:Column(
                   textDirection: TextDirection.rtl,
                   children: <Widget>[
                     Padding(padding:EdgeInsets.only(top: 5.0),
                         child:ListTile(
                           onTap:  ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())),
                           title: Column(
                             children: <Widget>[
                               CircleAvatar(
                                 backgroundColor: color,
                                 radius: 25.0,
                                 child: Image(image:AssetImage('img/logo.png',),width: 20.0,height: 20.0),),
                               Text('مركز النصر الرياضي',
                                 style:TextStyle(color:Colors.grey[800],fontFamily: "Cairo",fontWeight: FontWeight.w500) ,),
                               Row(
                                 mainAxisAlignment: MainAxisAlignment.center,
                                 children: <Widget>[
                                   Image(
                                     image: AssetImage(
                                         'img/baseline-star-24px.png'),
                                   ),
                                   Image(
                                     image: AssetImage(
                                         'img/baseline-star-24px.png'),
                                   ),
                                   Image(
                                     image: AssetImage(
                                         'img/baseline-star-24px.png'),
                                   ),
                                   Image(
                                     image: AssetImage(
                                         'img/baseline-star-24px.png'),
                                   ),
                                   Image(
                                     image: AssetImage('img/Path 1191.png'),
                                   ),
                                 ],
                               ),
                               Text('صباحي , مسائي'
                                   ,style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                               Text('1,2 كم',style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                             ],
                           ),

                           trailing:IconButton(
                             onPressed:() {
                               setState(() {
                                numbers.removeAt(index);
                               });
                             },
                             icon: Image(image: AssetImage('img/addfavourite.png'),),
                           ))),
                     Divider(color: Colors.grey,),
                     Container(
                       margin: EdgeInsets.only(right:10.0 ),
                       alignment: Alignment.topRight,
                       child:   Text('الخدمات',
                           style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, )),
                     ),
                     Container(
                         height: 30.0,
                         margin:  EdgeInsets.only(top: 10.0,bottom: 10.0),
                         child: ListView.builder(
                             scrollDirection: Axis.horizontal,
                             itemBuilder: (context,index) {
                               return Container(
                                 margin: EdgeInsets.only(left: 5.0, right: 5.0),
                                 width: 130.0,
                                 decoration: BoxDecoration(
                                     color: Colors.grey[400],
                                     borderRadius: BorderRadius.circular(15.0)
                                 ),
                                 child: Text('صالة كمال أجسام',
                                   style: TextStyle(
                                     color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w500,),
                                   textAlign: TextAlign.center,),
                               );
                             }))
                   ],
                 )),
           );
         })),
       /*Visibility(
         replacement:InkWell(
           onTap: showBtn,
           child:Container(
             alignment: Alignment.center,
             decoration: BoxDecoration(
               color: Colors.black54,
               borderRadius: BorderRadius.circular(15)
             ),
             margin: EdgeInsets.only(top: 200.0,left: 100.0,right: 100.0),
             width: 30.0,
             height:50.0 ,
             child: Text('تراجع عن الحذف',style: TextStyle(color: Colors.white,fontWeight: FontWeight.w500,fontFamily: 'Cairo'),)
           ),
         ),
         visible: _isVisible,
         child:Card(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child:Column(
              textDirection: TextDirection.rtl,
              children: <Widget>[
                Padding(padding:EdgeInsets.only(top: 5.0),
                    child:ListTile(
                      title: InkWell(
                        child: Column(
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor: color,
                              radius: 25.0,
                              child: Image(image:AssetImage('img/logo.png',),width: 20.0,height: 20.0),),
                            Text(' مركز النصر الرياضي 2',
                              style:TextStyle(color:Colors.grey[800],fontFamily: "Cairo",fontWeight: FontWeight.w500) ,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage('img/Path 1191.png'),
                                ),
                              ],
                            ),
                            Text('صباحي , مسائي'
                                ,style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                            Text('1,2 كم',style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                      trailing: IconButton(
                        onPressed:showBtn,

                        icon: Image(image: AssetImage('img/addfavourite.png'),),
                      ),
                    )),
                Divider(color: Colors.grey,),
                Container(
                  margin: EdgeInsets.only(right:10.0 ),
                  alignment: Alignment.topRight,
                  child:   Text('الخدمات',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, )),
                ),
                Container(
                    height: 30.0,
                    margin:  EdgeInsets.only(top: 10.0,bottom: 10.0),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context,index) {
                          return Container(
                            margin: EdgeInsets.only(left: 5.0, right: 5.0),
                            width: 130.0,
                            decoration: BoxDecoration(
                                color: Colors.grey[400],
                                borderRadius: BorderRadius.circular(15.0)
                            ),
                            child: Text('صالة كمال أجسام',
                              style: TextStyle(
                                color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w500,),
                              textAlign: TextAlign.center,),
                          );
                        }))
              ],
            ),
          )),*/


        ],
      ) ,

    ));
  }

}
