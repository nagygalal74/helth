import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/MyData.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  Color color=Color.fromARGB(242,12,82,123);
  TextEditingController oldPass;
  TextEditingController newPass;
  TextEditingController confirmPass;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        bottomOpacity: 0.0,
        toolbarOpacity: 0.0,
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
            padding: EdgeInsets.only(top: 5.0),
            icon: Image(image: AssetImage('img/backblack_@2x.png'),height: 40.0,width:30.0 ,),
            onPressed: ()=>Navigator.pop(context),
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.only(top: 10.0,right: 5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text('تغيير كلمة المرور',
                  style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),textScaleFactor: 2,),
                Container(
                  alignment: Alignment.topRight,
                  margin: EdgeInsets.only(top:25.0,right: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('كلمة المرور الفديمة',
                          style: TextStyle(color: Colors.grey[700],fontFamily: 'Cairo',fontWeight: FontWeight.w500,)),
                      Directionality(
                        textDirection:TextDirection.rtl,
                        child: TextFormField(
                            obscureText: true,
                             controller:oldPass,
                            decoration:InputDecoration(
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:Colors.transparent),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:Colors.transparent),
                              ),
                            )
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(color: Colors.grey,indent: 20.0,endIndent: 20.0,),
                Container(
                  alignment: Alignment.topRight,
                  margin: EdgeInsets.only(top:20.0,right: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text('كلمة المرور الجديدة',
                          style: TextStyle(color: Colors.grey[700],fontFamily: 'Cairo',fontWeight: FontWeight.w500,)),
                      Directionality(
                        textDirection:TextDirection.rtl,
                        child: TextFormField(
                            obscureText: true,
                             controller:newPass,
                            decoration:InputDecoration(
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:Colors.transparent),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:Colors.transparent),
                              ),
                            )
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(color: Colors.grey,indent: 20.0,endIndent: 20.0,height: 0,),
                Container(
                  alignment: Alignment.topRight,
                  margin: EdgeInsets.only(top:25.0,right: 15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(' تأكيد كلمة المرور الجديدة',
                          style: TextStyle(color: Colors.grey[700],fontFamily: 'Cairo',fontWeight: FontWeight.w500,)),
                      Directionality(
                        textDirection:TextDirection.rtl,
                        child: TextFormField(
                            obscureText: true,
                            controller:confirmPass,
                            decoration:InputDecoration(
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:Colors.transparent),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:Colors.transparent),
                              ),
                            )
                        ),
                      ),
                    ],
                  ),
                ),
                Divider(color: Colors.grey,indent: 20.0,endIndent: 20.0,height: 0,),
              ],
            ),
          ),
          InkWell(
            onTap: ()=>Navigator.pop(context),
            child: Container(
              height: 40.0,
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.symmetric(vertical: 5.0),
              margin: EdgeInsets.symmetric(vertical: 30.0,horizontal: 100),
              decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(20.0)
              ),
              child: Text('حفظ',style: TextStyle(color: Colors.white,fontFamily: 'Cairo',),
              ),
            ),
          ),
          InkWell(
            child:  Center(
            child: Text('نسيت كلمة المرور  ؟',
              style: TextStyle(backgroundColor: Colors.transparent,color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w800),)),
          )
        ],
      )
    );
  }
}
