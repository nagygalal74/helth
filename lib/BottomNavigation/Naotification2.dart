import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Notification.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/pages/Home.dart';
class notification extends StatefulWidget {
  @override
  _notificationState createState() => _notificationState();
}

class _notificationState extends State<notification> {
  Color color=Color.fromARGB(242,12,82,123);
  List numbers=[
    1,2,3
  ];

  @override
  Widget build(BuildContext context) {
    return  Directionality(textDirection:TextDirection.rtl,
    child:Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarOpacity: 0.0,
        elevation: 0.0,
        bottomOpacity: 0.0,
        title: Padding(padding: EdgeInsets.only(top: 10.0),
        child:Text('الإشعارات',style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,))),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          Divider(color: Colors.grey,),
          InkWell(
            onTap: ()=>Navigator.pop(context),
            child:  Container(
              height:MediaQuery.of(context).size.height*1,
              child:  ListView.builder(
                  padding: EdgeInsets.only(right:5.0,top: 15.0 ),
                  itemCount:numbers.length,
                  itemBuilder:(context,index){
                    // List<String>.generate(i, (i) => "Item ${i + 1}");
                    return Slidable(
                      delegate: SlidableDrawerDelegate(),
                      child: new Container(
                        color: Colors.white,
                        child: new ListTile(
                          title:  Row(

                            children: <Widget>[
                              Text('مركز النصر الرياضي',
                                style: TextStyle(color: Colors.black87,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:20 ),),
                              Padding(
                                  padding:
                                  EdgeInsets.only(right: 5.0, top: 5.0),
                                  child: Row(
                                    children: <Widget>[
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage(
                                            'img/baseline-star-24px.png'),
                                      ),
                                      Image(
                                        image: AssetImage('img/Path 1191.png'),
                                      ),
                                    ],
                                  )),
                            ],
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 15.0),
                                child: Text('تم تأكيد إشتراك مدة 3 شهور في مركز النصر الرياضي',
                                  style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500 ,),),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 0.0),
                                child: Text('2019/8/12',textDirection: TextDirection.rtl,
                                  style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500 ,),),
                              ),
                              Divider(color: Colors.grey,)
                            ],
                          ),
                        ),
                      ),
                      actions: <Widget>[
                        IconButton(icon: IconButton(icon: Image(image: AssetImage('img/Group 1196.png')), onPressed:(){
                          setState(() {
                            numbers.removeAt(index);
                          });
                        }),),
                      ],
                    );
                  }
              ),

            ),
          )
        ],
      ),
    ));
  }
}

