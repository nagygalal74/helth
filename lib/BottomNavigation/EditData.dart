import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/MyData.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
class EditData extends StatefulWidget {
  @override
  _EditDataState createState() => _EditDataState();
}

class _EditDataState extends State<EditData> {
  Color color=Color.fromARGB(242,12,82,123);
    TextEditingController name;
    TextEditingController phone;
    TextEditingController location;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              bottomOpacity: 0.0,
              toolbarOpacity: 0.0,
              elevation: 0.0,
              actions: <Widget>[
                IconButton(
                  padding: EdgeInsets.only(top: 5.0),
                  icon: Image(image: AssetImage('img/backblack_@2x.png'),height: 40.0,width:30.0 ,),
                  onPressed: ()=>Navigator.pop(context),
                ),
              ],
            ),
            body: SingleChildScrollView(
              child:  Container(
                height: MediaQuery.of(context).size.height*1,
                alignment: Alignment.topRight,
                margin: EdgeInsets.only(top: 5.0,right: 10.0),
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text('تعديل بياناتي',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),textScaleFactor: 2,),
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 25.0) ,
                        child:TextFormField(
                            controller: name,
                            decoration:InputDecoration(
                              hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14 ),
                              hintText: 'الاسم بالكامل',
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:color),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:color),
                              ),
                            )
                        ),
                      ),
                    ),
                    Padding(
                      padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 25.0) ,
                      child:Directionality(
                        textDirection:TextDirection.rtl,
                        child: TextFormField(
                            controller:phone,
                            keyboardType: TextInputType.number,
                            decoration:InputDecoration(
                              hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14 ),
                              hintText: 'رقم الجوال',
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:color),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:color),
                              ),
                            )
                        ),
                        //  )
                      ),
                    ),
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 25.0) ,
                        child:TextFormField(
                            controller:location,
                            decoration:InputDecoration(
                              hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14 ),
                              hintText: 'العنوان',
                              suffixIcon: IconButton(
                                  icon:Image(image: AssetImage('img/locations.png')),
                                  onPressed: (){}),
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:color),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color:color),
                              ),
                            )
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: ()=>Navigator.pop(context),
                      child: Container(
                        height: 40.0,
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.symmetric(vertical: 5.0),
                        margin: EdgeInsets.symmetric(vertical: 30.0,horizontal: 100),
                        decoration: BoxDecoration(
                            color: color,
                            borderRadius: BorderRadius.circular(20.0)
                        ),
                        child: Text('حفظ',style: TextStyle(color: Colors.white,fontFamily: 'Cairo',),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
    );
  }
}
