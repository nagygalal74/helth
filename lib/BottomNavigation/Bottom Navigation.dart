import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Naotification2.dart';
import 'package:healthyzone/BottomNavigation/Notification.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/pages/Home.dart';
class BottomNav extends StatefulWidget {


  @override
  _BottomNavState createState() => _BottomNavState();
}
class _BottomNavState extends State<BottomNav> {
  var _currentIndex=0;
    Widget page=Requests();
void onTabTapped(int index) {
    setState(() {
      _currentIndex =index;

    });
  }
  var  pages=[
   Home(),
    Requests(),
    NotificationPage(),
    Favourit(),
    PersonalData(),
  ];
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar:  Directionality(
          textDirection: TextDirection.rtl,
          child: BottomNavigationBar(
            onTap: onTabTapped,
            currentIndex: _currentIndex,
            backgroundColor: color,
            elevation: 0.0,
           unselectedItemColor: Colors.white30,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                  icon:Image(image: AssetImage('img/hometaplight@2x.png',),width: 30.0,color: _currentIndex==0?Colors.lightGreen:Colors.white30,),

                  title: Text('')
              ),
              BottomNavigationBarItem(
                 icon: Image(image: AssetImage('img/calendertap.png',),color: _currentIndex==1?Colors.lightGreen:Colors.white30,),

                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('img/notificationstap.png',),color: _currentIndex==2?Colors.lightGreen:Colors.white30,),

                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('img/favouritetap.png',),color: _currentIndex==3?Colors.lightGreen:Colors.white30,),

                  title: Text('')
              ),
              BottomNavigationBarItem(
                 icon: Image(image: AssetImage('img/profiletap.png',),color: _currentIndex==4?Colors.lightGreen:Colors.white30,),

                  title: Text('')
              ),


            ],
          )),
      body: pages[_currentIndex],
    );
  }
}
