import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/EditData.dart';
import 'package:healthyzone/BottomNavigation/EditHealthy.dart';
import 'package:healthyzone/BottomNavigation/EditLang.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Notification.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/BottomNavigation/changePassword.dart';
import 'package:healthyzone/drawer/Pocket.dart';
import 'package:healthyzone/drawer/about.dart';
import 'package:healthyzone/drawer/conditions.dart';
import 'package:healthyzone/drawer/contact.dart';
import 'package:healthyzone/drawer/join%20us.dart';
import 'package:healthyzone/drawer/privacy.dart';
import 'package:healthyzone/pages/Home.dart';
import 'package:healthyzone/pages/signup.dart';
class MyData extends StatefulWidget {
  @override
  _MyDataState createState() => _MyDataState();
}

class _MyDataState extends State<MyData> {
  Color color=Color.fromARGB(242,12,82,123);
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          backgroundColor: Colors.white,
          //====================================StartDrawer========= ========================================
          endDrawer:Drawer(
            elevation: 0.0,
            child: Container(
              color:color,
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding:EdgeInsets.only(top: 15.0,right: 10.0) ,
                    child:IconButton(
                      alignment:Alignment.topRight ,
                      onPressed:()=>Navigator.pop(context) ,
                      icon:Image(image: AssetImage('img/backblack_@2x.png'),
                          color: Colors.lightGreen,height: 30.0,width: 30.0),),),
                  Padding(padding:EdgeInsets.only(top: 15.0,right: 25.0),
                      child:Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          InkWell(
                            onTap: (){},
                            child:Text('مساعدة',
                                style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                          ),
                          Container(
                              margin: EdgeInsets.only(top: 20.0),
                              child:InkWell(
                                onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Contact())),
                                child:Text('اتصل بنا',
                                    style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                              )),
                          Container(
                              margin: EdgeInsets.only(top: 20.0,right: 14.0),
                              child:InkWell(
                                onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>About())),
                                child:Text('من نحن',
                                    style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                              )),
                          Container(
                              margin: EdgeInsets.only(top: 20.0,),
                              child:InkWell(
                                onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Privacy())),
                                child:Text('سياسة الخصوصية',
                                    style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                              )),
                          Container(
                              margin: EdgeInsets.only(top: 20.0,),
                              child:InkWell(
                                onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Conditions())),
                                child:Text('الشروط والأحكام',
                                    style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                              )),
                          Container(
                              margin: EdgeInsets.only(top: 20.0,),
                              child:InkWell(
                                onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Join_us())),
                                child:Text('الإنضمام معنا كشريك',
                                    style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                              )),
                        ],
                      )),
                  Padding(padding: EdgeInsets.only(top: 110.0),child: Divider(color: Colors.grey,),),
                  Column(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.only(right:25.0,top: 10.0,bottom: 10.0 ),
                            alignment: Alignment.topRight,
                            child:InkWell(
                              onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Pocket())),
                              child:Text('المحفظة',
                                  style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                            )),
                        Divider(
                          color: Colors.grey,
                        ),
                        Container(
                            padding: EdgeInsets.only(right:25.0,top: 10.0,bottom: 10.0 ),
                            alignment: Alignment.topRight,
                            child:InkWell(
                              onTap: (){},
                              child:Text('مشاركة التطبيق',
                                  style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                            )),
                      ]
                  ),
                ],
              ),
            ),
          ),
          //==========Start List View=================================
          body: Directionality(textDirection: TextDirection.rtl,
           child:ListView(
            children: <Widget>[
              Row(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(top: 15.0,right: 5.0),
                    alignment: Alignment.topRight,
                    child:  Builder(
                      builder: (context) => IconButton(
                        icon:Image(image: AssetImage('img/meuu@2x.png'),height: 25,width: 25,),
                        onPressed: () => Scaffold.of(context).openEndDrawer(),
                        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                      ),
                    )),
                Padding(padding:EdgeInsets.only(top: 15.0,right: 5.0),
                    child:Text('عبد الرحمن',
                    style: TextStyle(color:Colors.black87,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:17  ))),
              ],
              ),
              Divider(color: Colors.grey,),
              InkWell(
                  onTap: ()=>Navigator.pop(context),
                  child:Container(
                    child:Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              //==================================================بياناتي=====================================
                              Padding(padding:EdgeInsets.only(right: 15.0),
                                  child: Text('بياناتي',
                                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:15 ))),
                              Spacer(),
                              IconButton(
                                padding: EdgeInsets.only(left: 15.0),
                                icon:Image(image: AssetImage('img/edit.png')) ,
                                onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>EditData())),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10.0,top: 10.0,),
                            child:Row(
                              children: <Widget>[
                                Image(image: AssetImage('img/name@2x.png'),height: 27,width: 30,),
                                Padding(padding:EdgeInsets.only(right: 15.0) ,
                                    child: Text('عبد الرحمن العزب',
                                        style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ))),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10.0,top: 25.0,),
                            child:Row(
                              children: <Widget>[
                                Image(image: AssetImage('img/invoice@2x.png'),height: 27,width: 30,),
                                Padding(padding:EdgeInsets.only(right: 15.0) ,
                                    child: Text('+966094395923',
                                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ),
                                      textDirection: TextDirection.ltr ,)),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10.0,top: 25.0,bottom: 15.0),
                            child:Row(
                              children: <Widget>[
                                Image(image: AssetImage('img/invoicce@2x.png'),height: 27,width: 30,),
                                Padding(padding:EdgeInsets.only(right: 20.0) ,
                                    child: Text('العليا , الرياض',
                                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ),
                                      textDirection: TextDirection.ltr ,)),
                              ],
                            ),
                          ),
                          Divider(color: Colors.grey,),
                          Row(
                            children: <Widget>[
                              Padding(padding:EdgeInsets.only(right: 15.0),
                                  child: Text('صحتي',
                                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:15 ))),
                              Spacer(),
                              IconButton(
                                padding: EdgeInsets.only(left: 15.0),
                                icon:Image(image: AssetImage('img/edit.png')) ,
                                onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Edithealthy())),
                              ),
                            ],
                          ),
                          Container(
                            color: Colors.transparent,
                            margin: EdgeInsets.only(right: 10.0,top: 10.0,),
                            child:Row(
                              children: <Widget>[
                                Image(image: AssetImage('img/height.png'),),
                                Padding(padding:EdgeInsets.only(right: 15.0),
                                    child: Text('الطول',
                                        style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ))),
                                Container(
                                    width: 120.0,
                                    margin: EdgeInsets.only(right: 110.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text('180',
                                          style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold, ),
                                        ),
                                        Padding(padding:EdgeInsets.only(right: 10.0),
                                            child:Text('سم' ,  style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            color: Colors.transparent,
                            margin: EdgeInsets.only(right: 10.0,top: 20.0,),
                            child:Row(
                              children: <Widget>[
                                Image(image: AssetImage('img/weight.png'),),
                                Padding(padding:EdgeInsets.only(right: 15.0),
                                    child: Text('الوزن',
                                        style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ))),
                                Container(
                                    width: 120.0,
                                    margin: EdgeInsets.only(right: 115.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text('78',
                                          style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold, ),
                                        ),
                                        Padding(padding:EdgeInsets.only(right: 15.0),
                                            child:Text('كجم' ,  style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),))
                                      ],
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            color: Colors.transparent,
                            margin: EdgeInsets.only(right: 10.0,top: 20.0,bottom: 20.0),
                            child:Row(
                              children: <Widget>[
                                Image(image: AssetImage('img/job.png'),),
                                Padding(padding:EdgeInsets.only(right: 15.0,),
                                    child: Text('الوظيفة',
                                        style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ))),
                                Padding(padding:EdgeInsets.only(right: 90.0),
                                    child: Text('مهندس كمبيوتر', style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ))),
                              ],
                            ),
                          ),
                          Divider(color:Colors.grey),
                          //=================كلمه المرور=====================================================
                          Container(
                            margin: EdgeInsets.only(right: 10.0,bottom:5.0 ),
                            child: Row(
                              children: <Widget>[
                                Text('كلمة المرور',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800, )),
                                Spacer(),
                                IconButton(
                                  padding: EdgeInsets.only(left: 15.0),
                                  icon:Image(image: AssetImage('img/edit.png')) ,
                                  onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ChangePassword())),
                                ),
                              ],
                            ),
                          ),
                          Divider(color: Colors.grey,),
                          //===========================اللغه=============================================
                          Container(
                            margin: EdgeInsets.only(right: 15.0,bottom:10 ),
                            child: Row(
                              children: <Widget>[
                                Text('اللغة',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800, )),
                                Spacer(),
                                IconButton(
                                  padding: EdgeInsets.only(left: 15.0),
                                  icon:Image(image: AssetImage('img/edit.png')) ,
                                  onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>EditLang())),
                                ),
                              ],
                            ),
                          ),
                          Divider(color: Colors.grey,)
                        ]),
                  ),
              ),
              InkWell(
                onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Signup())),
                child: Container(
                  margin: EdgeInsets.only(right: 15.0,bottom:10 ,top: 10.0),
                  child: 
                    Text('تسجيل خروج',
                        style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800, )),
                ),
              ),
              Divider(color: Colors.grey,)
            ],
          ))
    );
  }
}
