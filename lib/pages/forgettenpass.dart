import 'package:flutter/material.dart';
import 'package:healthyzone/pages/login.dart';
import 'package:healthyzone/pages/verificationcode.dart';
class forgetPassword extends StatefulWidget {
  @override
  _forgetPasswordState createState() => _forgetPasswordState();
}

class _forgetPasswordState extends State<forgetPassword> {
  TextEditingController phoneNumber2;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color.fromARGB(242,12,82,123),
        //Color.fromARGB(203,10,94,135),
        body:ListView(
            children: <Widget>[
              Container(
                  width: 50.0,
                  height: 50.0,
                  child:Stack(
                    children: <Widget>[
                      Positioned(
                          top: 15.0,
                          right: 5.0,
                          child:InkWell(
                            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>loginPage())),
                            child:Container(
                              color: Colors.transparent,
                              child: Image(image: AssetImage('img/backblack_@2x.png'),
                                color: Colors.white,height: 40.0,width: 40.0,))
                          )),
                    ],
                  ),
              ),
              SizedBox(height: 70.0,),
              Center(
                child:Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    // SizedBox(height: 20.0,),
                    Image(image: AssetImage('img/logo.png',),width: 60.0,height: 60.0,),
                    SizedBox(height: 20.0,),
                    Text('نسيت كلمة المرور',
                      style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500)
                      ,textScaleFactor:1.2 ,),
                  ],
                ),
              ),
              SizedBox(height: 20.0,),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15.0) ,
                  child:TextFormField(
                    controller:phoneNumber2,
                      keyboardType: TextInputType.number,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:16  ),
                        hintText: 'رقم الجوال',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                      )),
                ),
              ),
              InkWell(
                onTap:()=>Navigator.push(context, MaterialPageRoute(builder:(context)=>Verification())),
                child: Container(
                   margin: EdgeInsets.only(left: 60.0,right: 60.0,top: 30.0),
                  width:MediaQuery.of(context).size.width*.38,
                  height: 38.0,
                  decoration: BoxDecoration(
                      color: Colors.lightGreen,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Text('إرسال',
                    style: TextStyle(color: Colors.white,fontFamily: "Cairo" ,),textAlign: TextAlign.center,),
                ),
              ),
    ]),
    );
  }
}