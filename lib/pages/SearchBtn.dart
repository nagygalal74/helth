import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
class SearchBtn extends StatefulWidget {
  @override
  _SearchBtnState createState() => _SearchBtnState();
}

class _SearchBtnState extends State<SearchBtn> {
  int sharedValue = 0;
  static Color color=Color.fromARGB(242,12,82,123);
 static bool viewVisible = true;
   hideWidget(){
    setState(() {
      viewVisible = false ;
    });
  }
   Map<int, Widget> Button =  <int, Widget>{
    0:Text('القائمة', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 15)),
     1:Text('الخريطة', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 15)),
  };
   Map<int,Widget> searchBtn = <int, Widget>{
  0:Container(
   height: 600,
    child:ListView(
      children: <Widget>[
        Card(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child:Column(
              textDirection: TextDirection.rtl,
              children: <Widget>[
                Padding(padding:EdgeInsets.only(top: 20.0),
               child:ListTile(
                  title: InkWell(
                    child: Column(
                      children: <Widget>[
                        CircleAvatar(
                          backgroundColor: color,
                          radius: 25.0,
                          child: Image(image:AssetImage('img/logo.png',),width: 20.0,height: 20.0),),
                        Text('مركز النصر الرياضي',
                          style:TextStyle(color:Colors.grey[800],fontFamily: "Cairo",fontWeight: FontWeight.w500) ,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image(
                              image: AssetImage(
                                  'img/baseline-star-24px.png'),
                            ),
                            Image(
                              image: AssetImage(
                                  'img/baseline-star-24px.png'),
                            ),
                            Image(
                              image: AssetImage(
                                  'img/baseline-star-24px.png'),
                            ),
                            Image(
                              image: AssetImage(
                                  'img/baseline-star-24px.png'),
                            ),
                            Image(
                              image: AssetImage('img/Path 1191.png'),
                            ),
                          ],
                        ),
                        Text('صباحي , مسائي'
                            ,style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                        Text('1,2 كم',style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                      ],
                    ),
                  ),
                  leading: IconButton(
                    onPressed:(){},

                    icon: Image(image: AssetImage('img/addfavourite.png'),color: Colors.grey,),
                  ),
                )),
                Divider(color: Colors.grey,),
                Container(
                  margin: EdgeInsets.only(right:10.0 ),
                  alignment: Alignment.topRight,
                  child:   Text('الخدمات',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, )),
                ),
                Container(
                    height: 30.0,
                    margin:  EdgeInsets.only(top: 10.0,bottom: 10.0),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context,index) {
                          return Container(
                            margin: EdgeInsets.only(left: 10.0, right: 10.0),
                            width: 130.0,
                            decoration: BoxDecoration(
                                color: Colors.grey[400],
                                borderRadius: BorderRadius.circular(15.0)
                            ),
                            child: Text('صالة كمال أجسام',
                              style: TextStyle(
                                color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w500,),
                              textAlign: TextAlign.center,),
                          );
                        }))
              ],
            ),
        ),
        Card(
            elevation: 5.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child:Column(
              textDirection: TextDirection.rtl,
              children: <Widget>[
                Padding(padding:EdgeInsets.only(top: 20.0),
                    child:ListTile(
                      title: InkWell(
                        child: Column(
                          children: <Widget>[
                            Container(
                              height: 60.0,
                          width: 60.0,
                          decoration:BoxDecoration(
                            border: Border.all(color: Colors.black),
                         shape: BoxShape.circle),
                          child:CircleAvatar(
                              foregroundColor: Colors.red,
                              backgroundColor:Colors.white,
                              radius: 30.0,
                              child: Image(image:AssetImage('img/header.png',),width: 35.0,height: 35.0),)),
                            Text('مركز الهلال الرياضي',
                              style:TextStyle(color:Colors.grey[800],fontFamily: "Cairo",fontWeight: FontWeight.w500) ,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage(
                                      'img/baseline-star-24px.png'),
                                ),
                                Image(
                                  image: AssetImage('img/Path 1191.png'),
                                ),
                              ],
                            ),
                            Text('صباحي , مسائي'
                                ,style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                            Text('1,2 كم',style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                          ],
                        ),
                      ),
                      leading: IconButton(
                        onPressed:(){
                         viewVisible=!viewVisible;
                          },

                        icon: Image(image: AssetImage('img/addfavourite.png'),color: Colors.grey,),
                      ),
                    )),
                Divider(color: Colors.grey,),
                Container(
                  margin: EdgeInsets.only(right:10.0 ),
                  alignment: Alignment.topRight,
                  child:   Text('الخدمات',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, )),
                ),
                Container(
                    height: 30.0,
                    margin:  EdgeInsets.only(top: 10.0,bottom: 10.0),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context,index) {
                          return Container(
                            margin: EdgeInsets.only(left: 10.0, right: 10.0),
                            width: 130.0,
                            decoration: BoxDecoration(
                                color: Colors.grey[400],
                                borderRadius: BorderRadius.circular(15.0)
                            ),
                            child: Text('صالة كمال أجسام',
                              style: TextStyle(
                                color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w500,),
                              textAlign: TextAlign.center,),
                          );
                        }))
              ],
            )
        ),
      Visibility(
        maintainSize: true,
        maintainAnimation: true,
        maintainState: true,
        visible: viewVisible,
        child:Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(top: 10.0,right: 10.0,left: 10.0,bottom: 10.0),
          color: Colors.grey,
          height:300.0,
         width: 300.0,
         child: Text('مساحة إعلانيه',style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 40.0),),
        )
      ),
        Card(
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child:Column(
            textDirection: TextDirection.rtl,
            children: <Widget>[
              Padding(padding:EdgeInsets.only(top: 20.0),
                  child:ListTile(
                    title: InkWell(
                      child: Column(
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: color,
                            radius: 25.0,
                            child: Image(image:AssetImage('img/logo.png',),width: 20.0,height: 20.0),),
                          Text('مركز النصر الرياضي',
                            style:TextStyle(color:Colors.grey[800],fontFamily: "Cairo",fontWeight: FontWeight.w500) ,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Image(
                                image: AssetImage(
                                    'img/baseline-star-24px.png'),
                              ),
                              Image(
                                image: AssetImage(
                                    'img/baseline-star-24px.png'),
                              ),
                              Image(
                                image: AssetImage(
                                    'img/baseline-star-24px.png'),
                              ),
                              Image(
                                image: AssetImage(
                                    'img/baseline-star-24px.png'),
                              ),
                              Image(
                                image: AssetImage('img/Path 1191.png'),
                              ),
                            ],
                          ),
                          Text('صباحي , مسائي'
                              ,style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                          Text('1,2 كم',style:TextStyle(color:Colors.grey,fontFamily: "Cairo",fontWeight: FontWeight.w500)),
                        ],
                      ),
                    ),
                    leading: IconButton(
                      onPressed:(){},

                      icon: Image(image: AssetImage('img/addfavourite.png'),color: Colors.grey,),
                    ),
                  )),
              Divider(color: Colors.grey,),
              Container(
                margin: EdgeInsets.only(right:10.0 ),
                alignment: Alignment.topRight,
                child:   Text('الخدمات',
                    style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, )),
              ),
              Container(
                  height: 30.0,
                  margin:  EdgeInsets.only(top: 10.0,bottom: 10.0),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context,index) {
                        return Container(
                          margin: EdgeInsets.only(left: 10.0, right: 10.0),
                          width: 130.0,
                          decoration: BoxDecoration(
                              color: Colors.grey[400],
                              borderRadius: BorderRadius.circular(15.0)
                          ),
                          child: Text('صالة كمال أجسام',
                            style: TextStyle(
                              color: color, fontFamily: 'Cairo', fontWeight: FontWeight.w500,),
                            textAlign: TextAlign.center,),
                        );
                      }))
            ],
          ),
        ),
      ],
    )
  )
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     // backgroundColor: Colors.white,
      appBar: AppBar(
          actions: <Widget>[
            IconButton(
                icon:Image(image: AssetImage('img/backblack.png'), height: 30.0,width: 30.0,),
                onPressed:()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>BottomNav()))),
          ],
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Container(
            height: 60.0,
            width: 200.0,
            margin: EdgeInsets.only(left: 60.0,right: 60.0),
            child:CupertinoSegmentedControl<int>(
              padding:EdgeInsets.only(right: 10.0,left: 10,top: 10.0,bottom: 10.0) ,
              selectedColor:color,
              borderColor: color,
              //pressedColor: color,
              children: Button,
              onValueChanged: (int val) {
                setState(() {
                  sharedValue = val;
                });
              },
              groupValue: sharedValue,
            )),
      ),
      body: ListView(
        children: <Widget>[
         Container(
             child:searchBtn[sharedValue]),
        ],
      ),
    );
  }
}
