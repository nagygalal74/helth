import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/pages/Centers.dart';
import 'package:healthyzone/pages/paymentmethod.dart';
class PartnerData extends StatefulWidget {
  @override
  _PartnerDataState createState() => _PartnerDataState();
}

class _PartnerDataState extends State<PartnerData> {
  Color color=Color.fromARGB(242,12,82,123);
  TextEditingController clubName;
  TextEditingController Date;
  TextEditingController requestNumber;
  TextEditingController requestDate;
  TextEditingController personNum;
  TextEditingController discount;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:ListView(
          children: <Widget>[
      Padding(
      padding:EdgeInsets.only(top: 15.0,right: 5.0) ,
      child:IconButton(
        alignment:Alignment.topRight ,
        onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())) ,
        icon:Image(image: AssetImage('img/backblack_@2x.png'),
            color:color,height: 30.0,width: 30.0),),),
            Padding(padding:EdgeInsets.only(top: 5.0,right: 15.0) ,
                child:Text('بيانات الإشتراك', style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 30.0,),
                    textAlign: TextAlign.end,)
            ),
            Column(
              children: <Widget>[
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                       controller:clubName,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'اسم النادي : نادي النصر العالمي',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        keyboardType:TextInputType.number,
                        controller:Date,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15,  ),
                          hintText: 'التاريخ : من 2019/10/10 الى 2019/10/30 ',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        controller:requestNumber,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'رقم الطلب : 100',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        controller:requestDate,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'تاريخ الطلب : 2019/10/1',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        controller:personNum,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'عدد الاشخاص:7',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Padding(padding:EdgeInsets.only(top: 20.0) ,
                    child:Text('اجمالي المبلغ',style: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:15  ),)
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(padding:EdgeInsets.only(top: 20.0,right: 9.0,left: 5.0) ,
                          child: Text('ريال',
                              style: TextStyle(color:Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:19.0 ))),
                      Text('1000',
                        style: TextStyle(color:Colors.grey[600],fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:40.0 ),),
                    ]),
              ],
            ),
            Directionality(
              textDirection: TextDirection.rtl,
              child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                child:TextFormField(
                    controller:discount,
                    decoration:InputDecoration(
                      hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                      hintText: 'كوبون الخصم',
                      enabledBorder:UnderlineInputBorder(
                        borderSide:BorderSide(color: color),
                      ),
                      focusedBorder:UnderlineInputBorder(
                        borderSide:BorderSide(color: color),
                      ),
                    )),
              ),
            ),
            InkWell(
              onTap: ()=>Navigator.pop(context),
              child: Container(
                margin: EdgeInsets.only(top: 20.0,right: 80.0,left: 80.0,bottom: 20.0),
                alignment: Alignment.center,
                height: 45.0,
                //width: 130.0,
                decoration: BoxDecoration(
                    color:color,
                    borderRadius: BorderRadius.circular(20.0)
                ),
                child: Text('مشاركة', style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
              ),
            ),
          ]),
        persistentFooterButtons: <Widget>[
    InkWell(
      onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>Payment())),
    child:Container(
        padding: EdgeInsets.only(top: 10.0),
    width:MediaQuery.of(context).size.width*1 ,
    height: 50.0,
    color:color,
    child: Text('تأكيد',
    style:TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,),textAlign: TextAlign.center),
    )),
    ]

    );
  }
}
