import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/pages/forgettenpass.dart';
import 'package:healthyzone/pages/newPassword.dart';
class Verification extends StatefulWidget {
  @override
  _VerificationState createState() => _VerificationState();
}

class _VerificationState extends State<Verification> {
  Timer _timer;
  int seconds;
  // Digital formatting, converting 0-9 time to 00-09
  String formatTime(int timeNum) {
    return timeNum < 10 ? "0" + timeNum.toString() : timeNum.toString();
  }

  // Time formatting, converted to the corresponding hh:mm:ss format according to the total number of seconds
  String constructTime(int seconds) {
    int hour = seconds ~/ 3600;
    int minute = seconds % 3600 ~/ 60;
    int second = seconds % 60;
    return  formatTime(minute) + ":" + formatTime(second);
  }
  @override
  void initState() {
    super.initState();
    // Get the current time
    var now = DateTime.now();
    // Get a 2-minute interval
    var twoHours = now.add(Duration(seconds: 30)).difference(now);
    // Get the total number of seconds, 2 minutes for 120 seconds
    seconds = twoHours.inSeconds;
    startTimer();
  }

  void startTimer() {
    // Set 1 second callback
    const period = const Duration(seconds: 1);
    _timer = Timer.periodic(period, (timer) {
      // Update interface
      setState(() {
        // minus one second because it calls back once a second
        seconds--;
      });
      if (seconds == 0) {
        // Countdown seconds 0, cancel timer
        cancelTimer();
      }
    });
  }

  void cancelTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
  }

  @override
  void dispose() {
    super.dispose();
    cancelTimer();

  }
  TextEditingController verification;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color.fromARGB(242,12,82,123),
        //Color.fromARGB(203,10,94,135),
        body:ListView(
            children: <Widget>[
              Container(
                width: 50.0,
                height: 50.0,
                child:Stack(
                  children: <Widget>[
                    Positioned(
                        top: 15.0,
                        right: 5.0,
                        child:InkWell(
                            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>forgetPassword())),
                            child:Container(
                                color: Colors.transparent,
                                child: Image(image: AssetImage('img/backblack_@2x.png'),
                                  color: Colors.white,height: 40.0,width: 40.0,))
                        )),
                  ],
                ),
              ),
              SizedBox(height: 50.0,),
              Center(
                child:Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    // SizedBox(height: 20.0,),
                    Image(image: AssetImage('img/logo.png',),width: 60.0,height: 60.0,),
                    SizedBox(height: 20.0,),
                    Text('رمز التحقق',
                      style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500)
                      ,textScaleFactor:1.2 ,),
                  ],
                ),
              ),
              SizedBox(height: 20.0,),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15.0) ,
                  child:TextFormField(
                      controller:verification,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:16  ),
                        hintText: 'رمز التحقق',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                      )),
                ),
              ),
              InkWell(
                onTap:()=>Navigator.push(context, MaterialPageRoute(builder:(context)=>newPassword())),
                child: Container(
                  margin: EdgeInsets.only(left: 60.0,right: 60.0,top: 30.0),
                  padding: EdgeInsets.only(top: 5.0),
                  width:MediaQuery.of(context).size.width*.28,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: Colors.lightGreen,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Text('إرسال',
                    style: TextStyle(color: Colors.white,fontFamily: "Cairo" ,),textAlign: TextAlign.center,),
                ),
              ),
              InkWell(
                onTap: (){},
                child: Container(
                  margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 22.0),
                  width:MediaQuery.of(context).size.width*.7,
                  height: 35.0,
                  child: Text('أعد إرسال كود التحقق مره أخرى',
                    style: TextStyle(color: Colors.lightGreen[300],fontFamily: "Cairo" ,fontWeight: FontWeight.w300,decoration:TextDecoration.underline ),
                    textAlign: TextAlign.center,),
                ),
              ),
              //=========================================Timer=================
              Container(
                margin: EdgeInsets.only(top: 10.0),
                child: Center(
                  child: Column(
                  mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text('سيصل إليك كود التحقق',style: TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w400),),
                      Text('في خلال'+ constructTime(seconds) +'ثانيه',
                        style:TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w500,wordSpacing:2.9  ) ,),
                    ],
                  ),
                ),
              )
     ]),
    );
  }
}
