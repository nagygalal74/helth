import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Favourit2.dart';
import 'package:healthyzone/BottomNavigation/Naotification2.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/drawer/Pocket.dart';
import 'package:healthyzone/drawer/about.dart';
import 'package:healthyzone/drawer/conditions.dart';
import 'package:healthyzone/drawer/contact.dart';
import 'package:healthyzone/drawer/join%20us.dart';
import 'package:healthyzone/drawer/privacy.dart';
import 'package:healthyzone/pages/Centers.dart';
import 'package:healthyzone/pages/SearchBtn.dart';
import 'package:healthyzone/testpage.dart';

import '../testpage.dart';
class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  DateTime _setDate = DateTime.now();
  DateTime _setDate2 = DateTime.now();
  int numPeople=1;
  int selectitem = 1;
  int duration=1;
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:color,
      //============================================Start Drawer===============================================================
      endDrawer: Drawer(
        elevation: 0.0,
        child: Container(
          color:color,
          child: ListView(
            children: <Widget>[
             Padding(
               padding:EdgeInsets.only(top: 15.0,right: 10.0) ,
            child:IconButton(
              alignment:Alignment.topRight ,
              onPressed:()=>Navigator.pop(context) ,
            icon:Image(image: AssetImage('img/backblack_@2x.png'),
              color: Colors.lightGreen,height: 30.0,width: 30.0),),),
              Padding(padding:EdgeInsets.only(top: 15.0,right: 25.0),
             child:Column(
               crossAxisAlignment: CrossAxisAlignment.end,
               //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
               children: <Widget>[
                 InkWell(
                   onTap: (){},
                   child:Text('مساعدة',
                       style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                 ),
                 Container(
                   margin: EdgeInsets.only(top: 20.0),
                   child:InkWell(
                   onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Contact())),
                   child:Text('اتصل بنا',
                       style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                 )),
                 Container(
                     margin: EdgeInsets.only(top: 20.0,right: 14.0),
                     child:InkWell(
                       onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>About())),
                       child:Text('من نحن',
                           style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                     )),
                 Container(
                     margin: EdgeInsets.only(top: 20.0,),
                     child:InkWell(
                       onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Privacy())),
                       child:Text('سياسة الخصوصية',
                           style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                     )),
                 Container(
                     margin: EdgeInsets.only(top: 20.0,),
                     child:InkWell(
                       onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Conditions())),
                       child:Text('الشروط والأحكام',
                           style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                     )),
                 Container(
                     margin: EdgeInsets.only(top: 20.0,),
                     child:InkWell(
                       onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Join_us())),
                       child:Text('الإنضمام معنا كشريك',
                           style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                     )),
               ],
                  )),
                Padding(padding: EdgeInsets.only(top: 110.0),child: Divider(color: Colors.grey,),),
                Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(right:25.0,top: 10.0,bottom: 10.0 ),
                      alignment: Alignment.topRight,
                        child:InkWell(
                          onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Pocket())),
                          child:Text('المحفظة',
                              style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                        )),
                    Divider(
                      color: Colors.grey,
                    ),
                    Container(
                        padding: EdgeInsets.only(right:25.0,top: 10.0,bottom: 10.0 ),
                        alignment: Alignment.topRight,
                        child:InkWell(
                          onTap: (){},
                          child:Text('مشاركة التطبيق', 
                              style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:18 )) ,
                        )),
                    ]
                ),
            ],
          ),
        ),
      ),
     //============================start List View=========================================================
     body: ListView(
       scrollDirection: Axis.vertical,
         physics:PageScrollPhysics(),

       children: <Widget>[
         Container(
           color: Colors.transparent,
           height: 90.0,
           width: 100.0,
           child: Stack(
             children: <Widget>[
              Positioned(
                top: 20.0,
                  right: 10.0,
                  child: Builder(
                 builder: (context) => IconButton(
                   icon:Image(image: AssetImage('img/meuu@3x.png'),height: 90.0,width: 100.0,),
                   onPressed: () => Scaffold.of(context).openEndDrawer(),
                   tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                 ),
               )),
               Positioned(
                 top: 25.0,
                   right: 50.0,
                   left: 30.0,
                   child:Image(image:AssetImage('img/logo.png',),width: 40.0,height: 40.0))
             ],
           )),
         Container(
           margin: EdgeInsets.only(right: 20.0,),
           child: Text(', مرحبا بك', style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:32 ),
           textAlign: TextAlign.end,),
         ),
         Container(
           margin: EdgeInsets.only(right: 20.0,),
           child: Text('إبحث عن ناديك المفضل', style: TextStyle(color: Colors.white70,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:20 ),
             textAlign: TextAlign.end,),
         ),
         Container(
           child: Column(
             children: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 20.0,left: 20.0,top: 10.0),
                  color: Colors.white,
                    child: ListTile(
                      onTap: (){},
                      trailing:Text('المنطقة',
                          style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                      leading: Icon(Icons.location_on,color:color ,),
                    ),
                ),
               //================= Container of Date=======================================================
               Container(
                 margin: EdgeInsets.only(top: 10.0,right: 20.0,left: 20.0),
                 child: Row(
                   children: <Widget>[
                     Expanded(
                         flex: 5,
                         //=============Bottom Sheet Of Date====================================================
                         child:Container(
                           alignment: Alignment.topRight,
                           color: Colors.white,
                           child: ListTile(
                             onTap: (){
                                showModalBottomSheet(
                                   context: context,
                                   builder: (BuildContext builder) {
                                     return  GestureDetector(
                                     onTap: ()=>Navigator.pop(context),
                                         child:Container(
                                         height: MediaQuery.of(context).copyWith().size.height / 3,
                                         child:CupertinoTheme(
                                         data: CupertinoThemeData(
                                         textTheme: CupertinoTextThemeData(
                                         dateTimePickerTextStyle: TextStyle(
                                         fontSize: 18,
                                           color: Colors.lightGreen
                                         ),
                                         ),
                                         ),child:CupertinoDatePicker(
                                           initialDateTime: DateTime.now(),
                                           onDateTimeChanged: (DateTime newdate2) {
                                             setState(() {
                                               _setDate2=newdate2;
                                             });
                                             print(newdate2);
                                           },
                                           minimumDate: DateTime(2020,1,1),
                                           maximumDate: new DateTime(2040, 1, 1),
                                           minimumYear: 2020,
                                           maximumYear:2040 ,
                                           mode: CupertinoDatePickerMode.date,
                                         ))));
                                   });
                             },
                               trailing: Text('التاريخ إلي',
                                   style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                               leading:Image(image: AssetImage('img/calendaradded@2x.png'),width: 30.0,height: 90.0,)
                           ),
                         )),
                    SizedBox(width: 20.0,),
                     //**********************====Bottom sheet of Date============**************===============
                     Expanded(
                         flex:5,
                         child:Container(
                           color: Colors.white,
                           child: ListTile(
                             onTap: (){
                               showModalBottomSheet(
                                   context: context,
                                   builder: (BuildContext builder) {
                                     return  GestureDetector(
                                         onTap: ()=>Navigator.pop(context),
                                     child:Container(
                                         height: MediaQuery.of(context).copyWith().size.height / 3,
                                         child:CupertinoTheme(
                                             data: CupertinoThemeData(
                                               textTheme: CupertinoTextThemeData(
                                                 dateTimePickerTextStyle: TextStyle(
                                                     fontSize: 18,
                                                     color: Colors.lightGreen
                                                 ),
                                               ),
                                             ),child:CupertinoDatePicker(
                                           initialDateTime: DateTime.now(),
                                           onDateTimeChanged: (DateTime newdate) {
                                             setState(() {
                                               _setDate=newdate;
                                             });
                                             print(newdate);
                                           },
                                           minimumDate: DateTime(2020,1,1),
                                           maximumDate: new DateTime(2040, 1, 1),
                                           minimumYear: 2020,
                                           maximumYear:2040 ,
                                           mode: CupertinoDatePickerMode.date,
                                         ))));
                                   });
                             },
                               trailing: Text('التاريخ من',
                                   style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                               leading:Image(image: AssetImage('img/calendaradded@2x.png'),width: 30.0,height: 90.0,)
                           ),
                         )),
                   ],
                 ),
               ),
               //*******====================Bottom Sheet Of Types OF Club**========
               Container(
                 margin: EdgeInsets.only(right: 20.0,left: 20.0,top: 10.0),
                 color: Colors.white,
                 child: ListTile(
                   onTap: (){
                     showModalBottomSheet(context: context, builder:(BuildContext builder){
                       return GestureDetector(
                       onTap: ()=>Navigator.pop(context),
                           child: Container(
                        // alignment:Alignment.topRight ,
                         height: MediaQuery.of(context).copyWith().size.height / 3,
                         child:Column(
                           crossAxisAlignment: CrossAxisAlignment.end,
                           children: <Widget>[
                            Padding(padding: EdgeInsets.only(right: 17.0,top: 20.0) ,
                           child: Text('نوع النادي',
                               style:TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:20 ))),
                             Expanded(
                                 child:CupertinoPicker(
                               itemExtent: 45.0,
                               magnification: 1.0,
                               backgroundColor: Colors.transparent,
                               onSelectedItemChanged: (int index) {
                                 selectitem = index;
                                },
                               children: <Widget>[
                                 Text('رجالي', style:TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                                 Text('نسائي', style:TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w700, ))
                               ],
                             ))
                           ],
                         ) ,
                       ));
                     });
                   },
                   trailing:Text('نوع النادي',
                       style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                   leading: Image(image: AssetImage('img/dumbbell.png'))
                 ),
               ),
               //=======Bottom sheet of num of people****==================================================================
               Container(
                 margin: EdgeInsets.only(right: 20.0,left: 20.0,top: 10.0),
                 color: Colors.white,
                 child: ListTile(
                     onTap: (){
                       showModalBottomSheet(context: context, builder:(BuildContext builder){
                         return GestureDetector(
                             onTap: ()=>Navigator.pop(context),
                             child: Container(
                               // alignment:Alignment.topRight ,
                               height: MediaQuery.of(context).copyWith().size.height / 3,
                               child:Column(
                                 crossAxisAlignment: CrossAxisAlignment.end,
                                 children: <Widget>[
                                   Padding(padding: EdgeInsets.only(right: 17.0,top: 20.0) ,
                                       child: Text('عددالأشخاص',
                                           style:TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:20 ))),
                                   Expanded(
                                       child:CupertinoPicker(
                                         itemExtent: 45.0,
                                         magnification: 1.0,
                                        // looping: true,
                                         backgroundColor: Colors.transparent,
                                         onSelectedItemChanged: (int index) {
                                        numPeople= index;
                                         },
                                         children: <Widget>[
                                           for( int numPeople=1; numPeople<=100;numPeople++ ) Text(numPeople.toString())
                                         ],
                                       ))
                                 ],
                               ) ,
                             ));
                       });


                     },
                     trailing:Text('عدد الأشخاص',
                         style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                     leading: Image(image: AssetImage('img/multiple-users-silhouette.png'))
                 ),
               ),
               //****=======================Bottom sheet Of Duration***====================================
               Container(
                 margin: EdgeInsets.only(right: 20.0,left: 20.0,top: 10.0),
                 color: Colors.white,
                 child: ListTile(
                     onTap: (){
                       showModalBottomSheet(context: context, builder:(BuildContext builder){
                         return GestureDetector(
                             onTap: ()=>Navigator.pop(context),
                             child:Container(
                               // alignment:Alignment.topRight ,
                               height: MediaQuery.of(context).copyWith().size.height / 3,
                               child:Column(
                                   crossAxisAlignment: CrossAxisAlignment.end,
                                   children: <Widget>[
                                     Padding(padding: EdgeInsets.only(right: 17.0,top: 20.0) ,
                                         child: Text('الفتره',
                                             style:TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:20 ))),
                                     Expanded(
                                       child:CupertinoPicker(
                                         itemExtent: 45.0,
                                         magnification: 1.0,
                                         backgroundColor: Colors.transparent,
                                         onSelectedItemChanged: (int index) {
                                           duration = index;
                                         },
                                         children: <Widget>[
                                           Container(
                                               child:Stack(
                                             children: <Widget>[
                                               Positioned(
                                                 top: 0.0,
                                             right: 15.0,
                                                   child: Text('صباحية',
                                               style:TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w700, ))
                                               ),
                                               Positioned(
                                                 top: 8.0,
                                                 right: 100,
                                                 child:Text('تبدا من افتتاح النادي الي الساعه2الظهر ',
                                                     style:TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:13 )
                                                 ),
                                               ),
                                 ],
                               )
                             ),
                                           Container(
                                               child:Stack(
                                                 children: <Widget>[
                                                   Positioned(
                                                       top: 0.0,
                                                       right: 15.0,
                                                       child: Text('مسائيه',
                                                           style:TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w700, ))
                                                   ),
                                                   Positioned(
                                                     top: 8.0,
                                                     right: 100,
                                                     child:Text('تبدا من 2الظهر الي 11 مساءا  ',
                                                         style:TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:13 )
                                                     ),
                                                   ),
                                                 ],
                                               )
                                           )
                          ])),
                         ])));
                       });
                     },
                     trailing:Text('الفترة',
                         style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                     leading: Image(image: AssetImage('img/clock.png'))
                 ),
               ),
               //==============================Button of Search==================================
               InkWell(
                 onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchBtn())),
                 child:Container(
                   padding: EdgeInsets.only(top: 10.0),
                   width:MediaQuery.of(context).size.width*1 ,
                   height: 50.0,
                 margin: EdgeInsets.only(right: 20.0,left: 20.0,top: 10.0),
                 color: Colors.lightGreen,
                 child: Text('بحث',
                     style:TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,),textAlign: TextAlign.center),
               )),
              Container(
                  margin: EdgeInsets.only(top: 10,right: 30.0,bottom:10.0),
                alignment: Alignment.topRight,
               child:Text('أحدث المراكز',
                    style:TextStyle(color: Colors.white70,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,fontSize:17.0 ))),
               Container(
                   height: 150.0,
                   //width: 200.0,
                   child:ListView.builder(
                       scrollDirection: Axis.horizontal,
                         itemBuilder: (context,index){
                         return InkWell(
                             onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())),
                           child:  Container(
                             width: 150.0,
                             margin: EdgeInsets.only(top: 15,left: 5.0,right: 5.0,bottom: 10.0),
                             color:Colors.white,
                             // height: 100.0,
                             //width: 100.0,
                             child:  Column(
                               children: <Widget>[
                                 Padding(padding:EdgeInsets.only(top: 7.0),
                                 child: CircleAvatar(
                                    backgroundColor: color,
                                    radius: 25.0,
                                    child: Image(image:AssetImage('img/logo.png',),width: 30.0,height: 30.0),)),
                                 Text('مركز النصر الرياضي',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700, )),
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image(image: AssetImage('img/Path 1191.png'),),
                                    Image(image: AssetImage('img/baseline-star-24px.png'),),
                                    Image(image: AssetImage('img/baseline-star-24px.png'),),
                                    Image(image: AssetImage('img/baseline-star-24px.png'),),
                                    Image(image: AssetImage('img/baseline-star-24px.png'),),
                                  ],
                                )
                               ],
                             )
                         ));
                       }
                   )),
             ],
           )),
    ])
    );
  }
}


