import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_star_rating/flutter_star_rating.dart';
import 'package:healthyzone/pages/SearchBtn.dart';
import 'package:healthyzone/pages/centerPics.dart';
import 'package:healthyzone/pages/partanerdata.dart';
import 'package:rating_dialog/rating_dialog.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
class centersPage extends StatefulWidget {
  @override
  _centersPageState createState() => _centersPageState();
}

class _centersPageState extends State<centersPage> {
  Color color=Color.fromARGB(242,12,82,123);
  bool _alreadySaved;
  double rating = 0.0;
  bool colors;


  @override
  void initState() {
    super.initState();
    colors=true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        bottomOpacity: 0.0,
        toolbarOpacity: 0.0,
        actions: <Widget>[
          Padding(padding: EdgeInsets.only(top: 20.0,right: 20.0) ,
              child:Text('مركز النصر الرياضي',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,))),
        Padding(padding: EdgeInsets.only(top: 10.0,right: 10.0) ,
            child: CircleAvatar(
            backgroundColor: color,
            radius: 20.0,
            child: Image(image:AssetImage('img/logo.png',),width: 25.0,height: 25.0),)),
    Padding(padding: EdgeInsets.only(top: 10.0,right: 5.0) ,
        child: InkWell(
          onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>SearchBtn())),
          child:Image(image: AssetImage('img/backblack_@2x.png'),
            color:color,height: 30.0,width: 30.0,) ,)),

        ],
        leading:Row(
          children: <Widget>[
           Expanded(
             flex: 5,
             child: Container (
                 width: 80,
                 //height: 100,
               margin: EdgeInsets.only(left: 8.0,top: 10),
                 child:InkWell(
                   onTap: (){
                     setState(() {
                       colors=!colors;
                     });
                   },
                   child: Image(image: AssetImage('img/favouritetap.png'),color:colors?Colors.grey:Colors.red,  ),
                 )
                 ),
            ),
        Expanded(
          flex: 5,
          child: Container(
           width: 80,
             //height: 80,
             margin: EdgeInsets.only(left: 5.0,top: 10),
         child:InkWell(
                  child: Image(image: AssetImage('img/baseline-share-24px.png'),)))),

          ],
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 250,
            width: 250,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 5,
                      child:InkWell(
                        onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>centerPic())),
                        child:Container(
                          width: 60,
                        height: 120,
                        child:Image(image: AssetImage('img/c12.jpg'),fit: BoxFit.fill,)) ,
                    )),
                    Expanded(
                      flex: 5,
                      child:InkWell(
                        onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>centerPic())),
                        child:Container(
                          width: 60,
                          height: 120.0,
                          child:Image(image: AssetImage('img/c1.jpg'),fit: BoxFit.fill,)) ,
                    )),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child:InkWell(
                        onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>centerPic())),
                        child:Container(
                          width: 60,
                          height: 120,
                          child:Image(image: AssetImage('img/c12.jpg'),fit: BoxFit.fill,)) ,
                    )),
                    Expanded(
                      flex: 3,
                      child:InkWell(
                        onTap:  ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>centerPic())),
                        child:Container(
                          width: 60,
                          height: 120.0,
                          child:Image(image: AssetImage('img/c3.jpg'),fit: BoxFit.fill,)) ,
                    )),
                    Expanded(
                      flex: 3,
                      child:InkWell(
                        onTap:  ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>centerPic())),
                        child:Container(
                          width: 60,
                          height: 120.0,
                          child:Image(image: AssetImage('img/c2.jpg'),fit: BoxFit.fill,)) ,
                    )),
                  ],
                ),
              ],
            ),
          ),
         Container(

           child:
    ListTile(
             title:Text('مركز النصر الرياضي',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),textAlign: TextAlign.end,),
             leading: Text('1.2 كم',
               style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textAlign:TextAlign.end,
               textDirection: TextDirection.rtl,),
              subtitle:Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(padding:EdgeInsets.only(top: 10.0) ,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Image(image: AssetImage('img/Path 1191.png'),),
                          Image(image: AssetImage('img/baseline-star-24px.png'),),
                          Image(image: AssetImage('img/baseline-star-24px.png'),),
                          Image(image: AssetImage('img/baseline-star-24px.png'),),
                          Image(image: AssetImage('img/baseline-star-24px.png'),),
                        ],
                      )),
             Padding(padding:EdgeInsets.only(top: 5.0) ,
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.end,
                     children: <Widget>[
                       Padding(padding:EdgeInsets.only(top: 10.0,right: 0.0) ,
                           child:Text('ريال/ يوم',
                               style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,))),
                       Text('35',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 25)),
                     ],
                    )
                   ),
                ],
              ),
           ),
         ),
      Padding(padding:EdgeInsets.only(top: 0.0,right: 17.0,bottom: 10.0) ,
        child:Text('صباحي,مسائي',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textAlign: TextAlign.end,)),
        //=========================الخدمات====================================
        Divider(color: Colors.grey[600],height: 25,indent: 20.0,endIndent: 20.0,),
      Padding(padding:EdgeInsets.only(bottom: 5.0,right: 17.0) ,
        child: Text('الخدمات',
              style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500, ),
        textAlign: TextAlign.end,)),
          Container(
           height: 30,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context,index) {
                    return InkWell(
                        onTap: () {},
                        child: Container(
                          margin: EdgeInsets.only(left: 10.0,right: 10.0),
                          //height: 100.0,
                          width: 130.0,
                        decoration: BoxDecoration(
                            color: Colors.grey[400],
                          borderRadius: BorderRadius.circular(10.0)
                        ),
                       child: Text('صالة كمال أجسام',
                         style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textAlign:TextAlign.center ,),
          )
      );
    }),
          ),
          Divider(color: Colors.grey[600],height: 20,indent: 20.0,endIndent: 20.0,),
      Container(
        child:
            ListTile(
              onTap:(){
                showModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      topLeft: Radius.circular(30),
                    ),
                  ),
                    isScrollControlled: true,
                    context: context, builder:(BuildContext bc){
                   return Container(
                     height: MediaQuery.of(context).size.height*.9,

                     child:Stack(
                             children: <Widget>[
                               Positioned(
                                 top: 10.0,
                                 right:10.0,
                                   child:IconButton(icon: Image(image: AssetImage('img/cancel.png'),),alignment:Alignment.topRight,
                                       onPressed:()=>Navigator.pop(context))),
                               Positioned(
                                 top: 50,
                                   right: 20.0,
                                   child:Text('وصف المركز',
                                     style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:15 ),textAlign:TextAlign.end ,)
                               ),
                               Positioned(
                                 top: 90.0,
                                 right: 20.0,
                                 left: 20.0,
                                   child:
                                   Text('هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق. هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.',
                                     style:TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textDirection: TextDirection.rtl,)
                               ),


                             ],
                           ),
                   );
                });
              },
              title: Text('وصف المركز',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textAlign:TextAlign.end ,),
              leading: Icon(Icons.arrow_back_ios,color: Colors.grey,),
            ) ,
          ),
          Divider(color: Colors.grey[600],height: 20,indent: 20.0,endIndent: 20.0,),
          //=========********************الشروط والاحكام**********==============================================================
          Container(
            child:
            ListTile(
              onTap:(){
                showModalBottomSheet(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30),
                      ),
                    ),
                    isScrollControlled: true,
                    context: context, builder:(BuildContext bc){
                  return Container(
                    height: MediaQuery.of(context).size.height*.9,

                    child:Stack(
                      children: <Widget>[
                        Positioned(
                            top: 10.0,
                            right:10.0,
                            child:IconButton(icon: Image(image: AssetImage('img/cancel.png'),),alignment:Alignment.topRight,
                                onPressed:()=>Navigator.pop(context))),
                        Positioned(
                            top: 50,
                            right: 20.0,
                            child:Text('الشروط والإحكام',
                              style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:15 ),textAlign:TextAlign.end ,)
                        ),
                        Positioned(
                            top: 90.0,
                            right: 20.0,
                            left: 20.0,
                            child:
                            Text('هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع. ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق. هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.',
                              style:TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textDirection: TextDirection.rtl,)
                        ),


                      ],
                    ),
                  );
                });
              },
              title: Text('الشروط والإحكام',style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textAlign:TextAlign.end ,),
              leading: Icon(Icons.arrow_back_ios,color: Colors.grey,),
            ) ,
          ),
          Divider(color: Colors.grey[600],height: 20,indent: 20.0,endIndent: 20.0,),
          //=========************تقييم*****=================================================================================
      Container(
            child:
            ListTile(
              onTap:(){
                showModalBottomSheet(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30),
                      ),
                    ),
                    context: context, builder:(BuildContext bc){
                  return Container(
                    alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height*.35,
                      child: Column(
                    children: <Widget>[
                      Padding(padding:EdgeInsets.only(top: 30.0),
                      child:Text('قيمنا وشاركنا رأيك',style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w700))),
                      Padding(padding:EdgeInsets.only(top: 30.0),
                          child:Text('تقيمك',style: TextStyle(color:Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w700))),
                  Padding(padding:EdgeInsets.only(top: 10.0),
                  //===================Rating Bar==========================================================================
                  child:FlutterRatingBar(
                        initialRating: 1,
                        fillColor: Colors.amber,
                        borderColor: Colors.amber.withAlpha(50),
                        allowHalfRating: true,
                        textDirection: TextDirection.rtl,
                        onRatingUpdate: (rating) {
                          print(rating);
                        },
                      )),
                      InkWell(
                        onTap: ()=>Navigator.pop(context),
                        child: Container(
                          margin: EdgeInsets.only(top: 10.0),
                          alignment: Alignment.center,
                          height: 50.0,
                         width: 130.0,
                          decoration: BoxDecoration(
                              color:color,
                            borderRadius: BorderRadius.circular(20.0)
                          ),
                          child: Text('مشاركة', style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
                        ),
                      ),
                    ],
                  ));
                    });
              },
              title: Text('هل قمت بزيارتنا ..شاركنا رأيك',
                style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textAlign:TextAlign.end ,),
              leading: Icon(Icons.arrow_back_ios,color: Colors.grey,),
            ) ,
          ),
        ],
      ),
      persistentFooterButtons: <Widget>[
        InkWell(
          onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>PartnerData())),
            child:Container(
              padding: EdgeInsets.only(top: 10.0),
              width:MediaQuery.of(context).size.width*1 ,
              height: 50.0,
              color:color,
              child: Text('إشتراك',
                  style:TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,),textAlign: TextAlign.center),
            )),

      ],
    );
  }
}
/*
 Directionality(textDirection:TextDirection.rtl ,
                      */