import 'package:custom_navigator/custom_navigation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/pages/Centers.dart';
import 'package:healthyzone/pages/Home.dart';
class paymentDone extends StatefulWidget {
  @override
  _paymentDoneState createState() => _paymentDoneState();
}

class _paymentDoneState extends State<paymentDone> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarOpacity: 0.0,
        bottomOpacity: 0.0,
        elevation: 0.0,
        centerTitle: true,
        title: Padding(padding: EdgeInsets.only(top: 20.0),child:   Image(image: AssetImage('img/header.png'),height: 40.0,width: 40,),),
       actions: <Widget>[
       // leading:
          Padding(padding: EdgeInsets.only(top: 15.0,right: 3.0),
              child: IconButton(
                alignment:Alignment.topRight ,
                onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())) ,
                icon:Image(image: AssetImage('img/backblack_@2x.png'),
                    color:color,height: 45.0,width: 45.0),)),
        ],
      ),
      body:ListView(
        children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 35.0,right: 20.0),
              child:Text('تم الإشتراك بنجاح',
            style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),
                textDirection: TextDirection.rtl,textScaleFactor:2 ,)),
          Padding(padding: EdgeInsets.only(top: 0.0,right: 20.0),
              child:Text('وتأكيد الدفع',
                style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),
                textDirection: TextDirection.rtl,textScaleFactor:2 ,)),
          Padding(padding: EdgeInsets.only(top:10.0,right: 20.0),
              child:Text('تم حفظ طلبك في الطلبات,,',
                style: TextStyle(color:Color.fromARGB(255,121,157,188),fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18),
                textDirection: TextDirection.rtl,)),
          Padding(padding: EdgeInsets.only(top:4.0,right: 20.0),
              child:Text('سيتم نقل الطلب إلي إشتراكاتي بتاريخ',
                style: TextStyle(color:Color.fromARGB(255,121,157,188),fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18),
                textDirection: TextDirection.rtl,)),
          Padding(padding: EdgeInsets.only(top:4.0,right: 20.0),
              child:Text('بداية الإشتراك',
                style: TextStyle(color:Color.fromARGB(255,121,157,188),fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18),
                textDirection: TextDirection.rtl,)),
          Padding(padding: EdgeInsets.only(top:4.0,right: 20.0),
              child:Text(' شكرا علي ثقتكم بنا',
                style: TextStyle(color:Color.fromARGB(255,121,157,188),fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18),
                textDirection: TextDirection.rtl,)),
        ],
      ),
      persistentFooterButtons: <Widget>[
        InkWell(
          onTap:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>BottomNav())),
            child:Container(
              margin: EdgeInsets.only(right: 10.0,left: 10.0),
              padding: EdgeInsets.only(top: 10.0),
              width:MediaQuery.of(context).size.width*1 ,
              height: 50.0,
              decoration: BoxDecoration(
                  color:color,
                borderRadius: BorderRadius.circular(10)
              ),
              child: Text('إنتقل إلي الطلبات',
                  style:TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,),textAlign: TextAlign.center),
            )),
        InkWell(
            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>BottomNav())),
            child:Container(
              margin: EdgeInsets.only(right: 10.0,left: 10.0,top: 10.0),
              padding: EdgeInsets.only(top: 10.0),
              width:MediaQuery.of(context).size.width*1 ,
              height: 50.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                border:Border.all(color: color)
              ),
              child: Text('الرئيسية',
                  style:TextStyle(color:color,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,),textAlign: TextAlign.center),
            )),
      ],
    );
  }
}
