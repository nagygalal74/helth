import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/pages/Centers.dart';
import 'package:healthyzone/pages/paymentdone.dart';
class Payment extends StatefulWidget {
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  static TextEditingController idnumber;
  static TextEditingController idName;
  static TextEditingController expired;
  static TextEditingController cvv;
  static TextEditingController value;
  Color color=Color.fromARGB(242,12,82,123);
  final Map<int, Widget> logoWidgets =  <int, Widget>{
    0: Container(

        child:Text('مدى', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w400, ))),
    1: Text('بطاقه إتمانيه', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w400, ),textAlign: TextAlign.center,),
    2: Text('كاش', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w400,)),
    3:  Text('المحفظة',style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w400,)),
  };
  final Map<int,Widget> payments = <int, Widget>{

    1: Container(

        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                      controller:idnumber ,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'رقم البطاقة',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                      controller:idName,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'إسم صاحب البطاقة',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                      controller: expired,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'تاريخ الإنتهاء',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                      controller: cvv,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'cvv',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                      controller: value,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'قيمة الشحن',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
            ])),
    //===================كاش**********************************************************
    2:Column(
      textDirection: TextDirection.rtl,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('تم تحديد طريقة الدفع كاش',
          style: TextStyle(color:Colors.grey[800],fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:18 ),),
    Padding(padding:EdgeInsets.only(right: 15.0,top: 5.0) ,
      child: Text('عند بداية الإشتراك',
        style: TextStyle(color:Colors.grey[800],fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:18 ),)),

      ],
    ),
    //=======================المحفظه***************************************************
    3:Column(
      textDirection: TextDirection.rtl,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('تم تحديد طريقة الدفع من المحفظة',
            style: TextStyle(color:Colors.grey[800],fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:18 ),),
       Padding(padding:EdgeInsets.only(right: 10.0) ,
         child: Text('سيتم خصم المبلغ من المحفظة',
          style: TextStyle(color:Colors.grey[800],fontFamily:'Cairo',fontWeight: FontWeight.w500, ),)),
    Padding(padding:EdgeInsets.only(right: 15.0,top: 10.0) ,
      child: Text('رصيدك   100.00  ريال',
        style: TextStyle(color:Colors.grey[800],fontFamily:'Cairo',fontWeight: FontWeight.w600, ),)),
      ],
    )
  };



  int sharedValue = 1;
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        toolbarOpacity: 0.0,
        bottomOpacity: 0.0,
        elevation: 0.0,
        centerTitle: true,
        title: Padding(padding: EdgeInsets.only(top: 20.0),child:   Image(image: AssetImage('img/header.png'),height: 40.0,width: 40,),),
        actions: <Widget>[
          Padding(padding: EdgeInsets.only(top: 15.0,right: 3.0),
              child: IconButton(
                alignment:Alignment.topRight ,
                onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())) ,
                icon:Image(image: AssetImage('img/backblack_@2x.png'),
                    color:color,height: 45.0,width: 45.0),)),
        ],
      ),
      body: ListView(
        children: <Widget>[
      Padding(padding: EdgeInsets.only(top:   24.0,right: 15),
        child:Text('إشترك الآن',
      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),textDirection: TextDirection.rtl,textScaleFactor: 2.5,)),
     Padding(padding: EdgeInsets.only(top:   0.0,right: 15),
      child:Text('إختر طريقة الدفع' ,style: TextStyle(color:Color.fromARGB(255,121,157,188)
        ,fontFamily: 'Cairo',fontWeight: FontWeight.w500,),textDirection:TextDirection.rtl ,textScaleFactor:1.9 ,)),
          Container(
            margin: EdgeInsets.only(right: 10.0,left: 10.0,top: 15.0),
              //height: 100.0,
              child:CupertinoSegmentedControl<int>(
                padding:EdgeInsets.only(right: 5.0,left: 5.0,top: 10.0,bottom: 5.0) ,
                selectedColor: Colors.lightGreen,
                borderColor:Colors.lightGreen,
                children:logoWidgets,
                onValueChanged: (int val) {
                  setState(() {
                    sharedValue = val;
                  });
                },
                groupValue: sharedValue,
              )),
          Container(
           // height: 200,
            //  width: 300,
            margin: EdgeInsets.only(top: 40.0,left: 10.0,right: 10.0),
            child: payments[sharedValue],
          ),

        ],
      ),
      persistentFooterButtons: <Widget>[
        InkWell(
            onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>paymentDone())),
            child:Container(
              padding: EdgeInsets.only(top: 10.0),
              width:MediaQuery.of(context).size.width*1 ,
              height: 50.0,
              color:color,
              child: Text('إشتراك',
                  style:TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w800,),textAlign: TextAlign.center),
            )),
      ],
    );
  }
}
