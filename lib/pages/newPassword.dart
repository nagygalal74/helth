import 'package:flutter/material.dart';
import 'package:healthyzone/pages/login.dart';
class newPassword extends StatefulWidget {
  @override
  _newPasswordState createState() => _newPasswordState();
}

class _newPasswordState extends State<newPassword> {
  TextEditingController newPass;
  TextEditingController newConfirm;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color.fromARGB(242,12,82,123),
        //Color.fromARGB(203,10,94,135),
        body:ListView(
            children: <Widget>[
              SizedBox(height: 80.0,),
              Center(
                child:Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    // SizedBox(height: 20.0,),
                    Image(image: AssetImage('img/logo.png',),width: 60.0,height: 60.0,),
                    SizedBox(height: 20.0,),
                    Text('كلمة مرور جديدة',
                      style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500)
                      ,textScaleFactor:1.2 ,),
                  ],
                ),
              ),
              SizedBox(height: 20.0,),
              Column(
                children: <Widget>[
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15) ,
                      child:TextFormField(
                          controller: newPass,
                          decoration:InputDecoration(
                            hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:16 ),
                            hintText: 'كلمة المرور الجديدة',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: Colors.white),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: Colors.white),
                            ),
                          )),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 14) ,
                      child:TextFormField(
                          controller: newConfirm,
                          decoration:InputDecoration(
                            hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:16),
                            hintText: 'تأكيد كلمة المرور الجديدة',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: Colors.white),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: Colors.white),
                            ),
                          )),
                    ),
                  ),
                ],
              ),
              InkWell(
                onTap:()=>Navigator.push(context, MaterialPageRoute(builder:(context)=>loginPage())),
                child: Container(
                  margin: EdgeInsets.only(left: 60.0,right: 60.0,top: 30.0),
                  padding: EdgeInsets.only(top: 5.0),
                  width:MediaQuery.of(context).size.width*.28,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: Colors.lightGreen,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Text('حفظ',
                    style: TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w800),textAlign: TextAlign.center,),
                ),
              ),
    ])
    );
  }
}
