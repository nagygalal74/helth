import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
import 'package:healthyzone/pages/login.dart';
class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  bool value1=false;
  TextEditingController name;
  TextEditingController phoneNumber;
  TextEditingController City;
  TextEditingController password;
  TextEditingController confirmPassword;
   _value1Changed(bool value) => setState(() => value1 = value);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Color.fromARGB(242,12,82,123),
      //Color.fromARGB(203,10,94,135),
      body:ListView(
        children: <Widget>[
          Container(
            width: 50.0,
           height: 50.0,
           child:Stack(
            children: <Widget>[
              Positioned(
                top: 15.0,
                left: 5.0,
                child:InkWell(
                  onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>BottomNav())),
                  child:Container(
                    color: Colors.transparent,
                    width: 80.0,
                    height: 80.0,
                    child: Text('تخطي',textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey[300],fontFamily: 'Cairo',fontWeight: FontWeight.w300,fontSize: 16),),
                ),
              )),
            ],
          )
          ),
          SizedBox(height: 20.0,),
          Center(
            child:Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
               // SizedBox(height: 20.0,),
               Image(image: AssetImage('img/logo.png',),width: 60.0,height: 60.0,),
                SizedBox(height: 20.0,),
                Text('تسجيل حساب جديد',
                  style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500)
                  ,textScaleFactor:1.2 ,),
              ],
            ),
          ),
          SizedBox(height: 20.0,),
          Column(
           // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                      child:TextFormField(
                        controller: name,
                      decoration:InputDecoration(
                          hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w200,fontSize:14 ),
                          hintText: 'الاسم بالكامل',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: Colors.white),
                          ),
                        focusedBorder:UnderlineInputBorder(
                      borderSide:BorderSide(color: Colors.white),
                      ),
                      )),
                  ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15.0) ,
                  child:TextFormField(
                      controller:phoneNumber,
                      keyboardType: TextInputType.number,
                      decoration:InputDecoration(
                          hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w200,fontSize:14  ),
                          hintText: 'رقم الجوال',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: Colors.white),
                          ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15.0) ,
                  child:TextFormField(
                      controller:City,
                      decoration:InputDecoration(
                        suffixIcon: Icon(Icons.location_on,color: Colors.white,),
                          hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w200,fontSize:14  ),
                          hintText: 'المدينة',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: Colors.white),
                          ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15) ,
                  child:TextFormField(
                      controller: password,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w200,fontSize:14 ),
                        hintText: 'كلمة المرور',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15) ,
                  child:TextFormField(
                      controller: confirmPassword,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w200,fontSize:14 ),
                        hintText: 'تأكيد كلمة المرور',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color: Colors.white),
                        ),
                      )),
                ),
              ),
           Container(
        width:  MediaQuery.of(context).size.width*.48,
            margin: EdgeInsets.only(
                top: 8.0
            ),
            child:CheckboxListTile(
              selected: false,

              activeColor: Colors.lightGreen,
              controlAffinity: ListTileControlAffinity.trailing,
                 value:value1 ,
                 onChanged: _value1Changed,
                 title: Text('الشروط والأحكام',
                   style: TextStyle(color: Colors.lightGreen,fontFamily: "Cairo" ,decoration:TextDecoration.underline ),),
             )),
            InkWell(
              onTap:(){
                setState(() {});
              },
              child: Container(
                margin: EdgeInsets.only(left: 20.0,right: 20.0),
                width:MediaQuery.of(context).size.width*.7,
                height: 35.0,
                decoration: BoxDecoration(
                  color: Colors.lightGreen,
                  borderRadius: BorderRadius.circular(10)
                ),
                child: Text('تسجيل',
                  style: TextStyle(color: Colors.white,fontFamily: "Cairo" ,),textAlign: TextAlign.center,),
              ),
            ),
              InkWell(
                autofocus: false,
                onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>loginPage())),
                child: Container(
                   margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 10.0),
                  width:MediaQuery.of(context).size.width*.7,
                  height: 35.0,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.lightGreen)
                  ),
                  child: Text('لديك حساب',
                    style: TextStyle(color: Colors.lightGreen,fontFamily: "Cairo" ,),textAlign: TextAlign.center,),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
