import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
import 'package:healthyzone/pages/forgettenpass.dart';
import 'package:healthyzone/pages/signup.dart';
class loginPage extends StatefulWidget {
  @override
  _loginPageState createState() => _loginPageState();
}

class _loginPageState extends State<loginPage> {
  TextEditingController phoneNumber;
  TextEditingController password;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:Color.fromARGB(242,12,82,123),
        //Color.fromARGB(203,10,94,135),
        body:ListView(
            children: <Widget>[
              Container(
                  width: 50.0,
                  height: 50.0,
                  child:Stack(
                    children: <Widget>[
                      Positioned(
                          top: 15.0,
                          left: 5.0,
                          child:InkWell(
                            onTap:()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>BottomNav())),
                            child:Container(
                              color: Colors.transparent,
                              width: 80.0,
                              height: 80.0,
                              child: Text('تخطي',textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.grey[300],fontFamily: 'Cairo',fontWeight: FontWeight.w300,fontSize: 16),),
                            ),
                          )),
                    ],
                  )
              ),
              SizedBox(height: 40.0,),
              Center(
                child:Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    // SizedBox(height: 20.0,),
                    Image(image: AssetImage('img/logo.png',),width: 60.0,height: 60.0,),
                    SizedBox(height: 20.0,),
                    Text('تسجيل دخول',
                      style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500)
                      ,textScaleFactor:1.5 ,),
                  ],
                ),
              ),
              SizedBox(height: 20.0,),
              Column(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15.0) ,
                        child:TextFormField(
                            controller:phoneNumber,
                            keyboardType: TextInputType.number,
                            decoration:InputDecoration(
                              hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14  ),
                              hintText: 'رقم الجوال',
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color: Colors.white),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color: Colors.white),
                              ),
                            )),
                      ),
                    ),
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 15) ,
                        child:TextFormField(
                            controller: password,
                            decoration:InputDecoration(
                              hintStyle: TextStyle(color:Colors.white,fontFamily:'Cairo',fontWeight: FontWeight.w500,fontSize:14 ),
                              hintText: 'كلمة المرور',
                              enabledBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color: Colors.white),
                              ),
                              focusedBorder:UnderlineInputBorder(
                                borderSide:BorderSide(color: Colors.white),
                              ),
                            )),
                      ),
                    ),
                    InkWell(
                      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>forgetPassword())),
                      child: Container(
                        margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 20.0),
                        width:MediaQuery.of(context).size.width*.7,
                        height: 35.0,
                        child: Text('نسيت كلمة المرور  ؟',
                          style: TextStyle(color: Colors.lightGreen,fontFamily: "Cairo" ,fontWeight: FontWeight.w700),textAlign: TextAlign.center,),
                      ),
                    ),
                    InkWell(
                      onTap:(){},
                      child: Container(
                        margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 10.0),
                        width:MediaQuery.of(context).size.width*.66,
                        height: 35.0,
                        decoration: BoxDecoration(
                            color: Colors.lightGreen,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: Text('تسجيل دخول',
                          style: TextStyle(color: Colors.white,fontFamily: "Cairo" ,fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
                      ),
                    ),
                    InkWell(
                      autofocus: false,
                      onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>Signup())),
                      child: Container(
                        margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 20.0),
                        width:MediaQuery.of(context).size.width*.66,
                        height: 35.0,
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.lightGreen)
                        ),
                        child: Text('ليس لديك حساب',
                          style: TextStyle(color: Colors.lightGreen,fontFamily: "Cairo" ,),textAlign: TextAlign.center,),
                      ),
                    ),
      ])
     ])
    );
  }
}
