import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/pages/Centers.dart';
class centerPic extends StatefulWidget {
  @override
  _centerPicState createState() => _centerPicState();
}

class _centerPicState extends State<centerPic> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:AppBar(
        backgroundColor:Colors.white,
        toolbarOpacity: 0.0,
        bottomOpacity: 0.0,
        elevation: 0.0,
        actions: <Widget>[
      Padding(padding: EdgeInsets.only(top: 20.0,right: 3.0),
      child: Text('صور المراكز',
            style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w700),textDirection: TextDirection.rtl,)),
      Padding(
          padding: EdgeInsets.only(top: 15.0,right: 0.0),
        child: IconButton(
          alignment:Alignment.topRight ,
          onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())) ,
          icon:Image(image: AssetImage('img/backblack_@2x.png'),
              color:color,height: 45.0,width: 45.0),)),

    ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height*1,
            width: MediaQuery.of(context).size.width*1,
            child: ListView.builder(
                itemBuilder:(context,i){
                  return InkWell(
                    onTap: (){
                      return Navigator.push(context,MaterialPageRoute(builder: (context){
                        return Scaffold(
                          backgroundColor: Colors.black,
                          appBar: AppBar(
                            backgroundColor:Colors.black,
                            toolbarOpacity: 0.0,
                            bottomOpacity: 0.0,
                            elevation: 0.0,
                            actions: <Widget>[
                              Padding(padding: EdgeInsets.only(top: 15.0,right: 3.0),
                                  child: IconButton(
                                    alignment:Alignment.topRight ,
                                    onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>centersPage())) ,
                                    icon:Image(image: AssetImage('img/backblack_@2x.png'),
                                        color:Colors.white,height: 45.0,width: 45.0),)),
                            ],
                          ),
                          body: Center(
                            child:Container(
                              margin: EdgeInsets.only(top: 10.0),
                              height: 250,
                              width:MediaQuery.of(context).size.width*1,
                              child: Image(image: AssetImage('img/c1.jpg'),fit: BoxFit.fill,),
                            ),
                          ),
                        );
                      }));
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 10.0),
                      height: 250,
                      width: MediaQuery.of(context).size.width*1,
                      child: Image(image: AssetImage('img/c1.jpg'),fit: BoxFit.fill,),
                    ),
                  ) ;
                  
                } ),
          )

        ],
      ),
    );
  }
}
