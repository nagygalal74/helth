import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
import 'package:healthyzone/pages/signup.dart';
class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  Color color=Color.fromARGB(242,12,82,123);
  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      padding: EdgeInsets.only(right: 20.0),
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: isActive ? 15: 15,
      width: isActive ? 40.0 : 15,
      decoration: BoxDecoration(
        color: isActive ? Colors.lightGreen : Colors.lightGreen[300],
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:color ,
        body: ListView(
            padding: EdgeInsets.only(top: 45.0),
            children: <Widget>[
              Row(
                textDirection: TextDirection.rtl,
                children: <Widget>[
                  InkWell(
                    onTap:(){
                      showModalBottomSheet(
                          backgroundColor: Colors.transparent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                            ),
                          ),
                          context: context, builder:(BuildContext bc){
                        return Container(
                            height: 200.0,
                            child:Column(
                              children: <Widget>[
                                Container(
                                    padding: EdgeInsets.symmetric(vertical: 10.0),
                                    margin: EdgeInsets.only(left: 10.0,right: 10.0),
                                    width: MediaQuery.of(context).size.width*1,
                                    height: 100.0,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(20.0)
                                    ),
                                    child: Column(
                                      children: <Widget>[
                                        InkWell(
                                          onTap: (){},
                                          child:
                                          Text('عربي',textAlign: TextAlign.center,
                                            style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 17.0),),

                                        ),
                                        Divider(color: Colors.grey,),
                                        InkWell(
                                          onTap: (){},
                                          child:
                                          Text('English',textAlign: TextAlign.center,
                                            style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 17.0),),
                                        ),
                                      ],
                                    )),
                                SizedBox(height: 30.0,),
                                Container(
                                    padding: EdgeInsets.symmetric(vertical: 10.0),
                                    margin: EdgeInsets.only(left: 10.0,right: 10.0),
                                    width: MediaQuery.of(context).size.width*1,
                                    height: 50.0,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(20.0)
                                    ),
                                    child:InkWell(
                                        onTap: (){},
                                        child:
                                        Text('إلغاء',textAlign: TextAlign.center,
                                          style: TextStyle(color: color,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 17.0),)
                                    )),
                              ],
                            ) );
                      }
                      );
                    },
                    child: Container(
                        alignment: Alignment.center,
                        width: 95.0,
                        height: 30.0,
                        padding: EdgeInsets.only(left: 5.0,),
                        margin: EdgeInsets.only(right: 15.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20.0),
                            color: Colors.lightGreen
                        ),
                        //alignment: Alignment.topRight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          textDirection: TextDirection.rtl,
                          children: <Widget>[
                            Image(image: AssetImage('img/noun_world_89588.png')),
                            Text('عربي',style: TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w700),
                              textAlign: TextAlign.center,)
                          ],
                        )
                    ),
                  ),
                  Spacer(),
                  InkWell(
                    onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>BottomNav())),
                    child:Container(
                      margin: EdgeInsets.only(left: 15.0),
                      color: Colors.transparent,
                      // width: 80.0,
                      //height: 80.0,
                      child: Text('تخطي',textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.grey[300],fontFamily: 'Cairo',fontWeight: FontWeight.w300,fontSize: 16),),
                    ),
                  )
                ],
              ),
             Padding(padding:EdgeInsets.only(top: 10.0,),
               child: Container(
                 alignment: Alignment.center,
                 height: 450.0,
                 child:Center(
                   child:PageView(
                     scrollDirection: Axis.horizontal,
                   controller: _pageController,
                   onPageChanged: (int page) {
                     setState(() {
                       _currentPage = page;
                     });
                   },
                   children: <Widget>[
                     Column(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: <Widget>[
                           Padding(padding: EdgeInsets.only(top: 40.0) ,
                               child: Text('اللياقة البدنية',
                                 style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight:FontWeight.w700,fontSize: 18 ),)),
                           Container(
                             margin: EdgeInsets.only(top: 20.0),
                             child:Column(
                               children: <Widget>[
                                 Text('ستجد مراكز رياضية لدينا وصالات كمال الأجسام',textDirection: TextDirection.rtl,
                                   style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,),
                                 Center(
                                     child:Padding(padding:EdgeInsets.only(right: 20.0,left: 20.0) ,
                                         child: Text('وألعاب وتمرينات رياضية لتحسين لياقتك',textAlign: TextAlign.center,
                                           style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,))),
                                 Center(
                                   child: Text('لبدنية',textDirection: TextDirection.rtl,
                                     style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,),
                                 ),
                                 Padding(padding:EdgeInsets.only(top: 40.0),
                                   child: Image.asset("img/Group 426.png",),
                                 ),
                               ],
                             ),
                           ),
                         ]),
                     Column(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: <Widget>[
                           Padding(padding: EdgeInsets.only(top: 40.0) ,
                               child: Text('تابع صحتك',
                                 style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight:FontWeight.w700,fontSize: 18 ),)),
                           Container(
                             margin: EdgeInsets.only(top: 20.0),
                             child:Column(
                               children: <Widget>[
                                 Text('قم بمتابعة وزنك وصحتك من خلال برامج',textDirection: TextDirection.rtl,
                                   style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,),
                                 Center(
                                     child:Padding(padding:EdgeInsets.only(right: 20.0,left: 20.0) ,
                                         child: Text('المراكز الرياضية و أنظمتها الغذائية',textAlign: TextAlign.center,
                                           style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,))),
                                 Center(
                                   child: Text('التي تناسب الجميع',textDirection: TextDirection.rtl,
                                     style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,),
                                 ),
                                 Padding(padding:EdgeInsets.only(top: 40.0),
                                   child: Image.asset("img/vector.png",),
                                 ),
                               ],
                             ) ,
                           )
                         ]),
                     Column(
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: <Widget>[
                           Padding(padding: EdgeInsets.only(top: 40.0) ,
                               child: Text('إضبط مواعيدك',
                                 style: TextStyle(color: Colors.lightGreen,fontFamily: 'Cairo',fontWeight:FontWeight.w700,fontSize: 18 ),)),
                           Container(
                             margin: EdgeInsets.only(top: 20.0),
                             child:Column(
                               children: <Widget>[
                                 Text('قم بتحديد المواعيد المناسبة للإشتراك',textDirection: TextDirection.rtl,
                                   style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,),
                                 Center(
                                     child:Padding(padding:EdgeInsets.only(right: 20.0,left: 20.0) ,
                                         child: Text('صباحا و مساءا مع المراكز الرياضية التي',textAlign: TextAlign.center,
                                           style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,))),
                                 Center(
                                   child: Text('تتناسب مع مواعيدك',textDirection: TextDirection.rtl,
                                     style:TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w500,) ,),
                                 ),
                                 Padding(padding:EdgeInsets.only(top: 40.0),
                                   child: Image.asset("img/vector_t.png",),
                                 ),

                               ],
                             ) ,
                           )
                         ]),
                   ],
                 )) ,
               ),
             ),
             FlatButton(
               highlightColor: Colors.transparent,
               onPressed:(){_pageController.nextPage(
                 duration: Duration(milliseconds: 500),
                 curve: Curves.ease,
               );},
                 child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: _buildPageIndicator(),
              )),
              SizedBox(height:25.0),
              _currentPage != _numPages - 1?
              InkWell(
                onTap: () {
                  _pageController.nextPage(
                    duration: Duration(milliseconds: 500),
                    curve: Curves.ease,
                  );
                },
                  child:Container(
                    color: Colors.transparent,
                child:Text('التالي',textAlign: TextAlign.center,
                  style:
                  TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w700 ),),) ,
              ):InkWell(
                onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Signup())),
                child:Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.symmetric(horizontal: 130.0),
                  height: 40.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    color: Colors.lightGreen,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child:Text('إبدأ',textAlign: TextAlign.center,
                    style:
                    TextStyle(color: Colors.white,fontFamily: 'Cairo',fontWeight:FontWeight.w700 ),),) ,
              )
            ])
    );

  }
}
