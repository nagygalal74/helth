import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/drawer/confirmCharge.dart';
import 'Pocket.dart';
class Charge_blance extends StatefulWidget {
  @override
  _Charge_blanceState createState() => _Charge_blanceState();
}

class _Charge_blanceState extends State<Charge_blance> {

  static TextEditingController idnumber;
  static TextEditingController idName;
  static TextEditingController expired;
  static TextEditingController cvv;
  static TextEditingController value;

  Color color=Color.fromARGB(242,12,82,123);
  final Map<int, Widget> logoWidgets =  <int, Widget>{
    0: Text('مدى', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 17)),
    1: Text('البطاقه الإئتمانيه', style: TextStyle(fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize: 17)),
  };

  final Map<int,Widget> textfield = <int, Widget>{

    1: Container(

        child: Column(
          // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                    controller:idnumber ,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'رقم البطاقة',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                    controller:idName,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'إسم صاحب البطاقة',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                    controller: expired,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'تاريخ الإنتهاء',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                    controller: cvv,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'cvv',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
              Directionality(
                textDirection: TextDirection.rtl,
                child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0) ,
                  child:TextFormField(
                    controller: value,
                      decoration:InputDecoration(
                        hintStyle: TextStyle(color:Color.fromARGB(242,12,82,123),fontFamily:'Cairo',fontWeight: FontWeight.w700,fontSize:14 ),
                        hintText: 'قيمة الشحن',
                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                        focusedBorder:UnderlineInputBorder(
                          borderSide:BorderSide(color:Color.fromARGB(242,12,82,123)),
                        ),
                      )),
                ),
              ),
            ])),
    };

  int sharedValue = 1;

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            toolbarOpacity: 0.0,
            bottomOpacity: 0.0,
            elevation: 0.0,
            centerTitle: true,
            title: Padding(padding: EdgeInsets.only(top: 20.0),child:   Image(image: AssetImage('img/header.png'),height: 40.0,width: 40,),),
            actions: <Widget>[
          Padding(padding: EdgeInsets.only(top: 15.0,right: 3.0),
             child: IconButton(
          alignment:Alignment.topRight ,
            onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Pocket())) ,
            icon:Image(image: AssetImage('img/backblack_@2x.png'),
                color:color,height: 35.0,width: 35.0),)),
              ],
          ),
        body: ListView(
              children: <Widget>[
                Padding(padding:EdgeInsets.only(top: 22.0,right: 15.0),
                    child:Text('شحن رصيد',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 28.0),textAlign: TextAlign.end,)),
      Container(
        height: 100.0,
      margin: EdgeInsets.only(left: 60.0,right: 60.0),
      child:CupertinoSegmentedControl<int>(
        padding:EdgeInsets.only(right: 10.0,left: 10,top: 10.0,bottom: 10.0) ,
       selectedColor: color,
        borderColor: color,
       children: logoWidgets,
       onValueChanged: (int val) {
       setState(() {
       sharedValue = val;
       });
     },
     groupValue: sharedValue,
      )),

     Container(
       height: 300,
     //  width: 300,
       child: textfield[sharedValue],
   ),
                Container(
                    height:MediaQuery.of(context).size.height*.19,
                    width: 70,
                    child:Stack(
                      children: <Widget>[
                        Positioned(
                          right:MediaQuery.of(context).size.height*0.0,
                          left:MediaQuery.of(context).size.height*0.0,
                          bottom: MediaQuery.of(context).size.height*0.0,
                          child:InkWell(
                            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>confirmCharge())),
                            child: Container(
                              alignment: Alignment.center,
                              height: 55.0,
                              color:color,
                              child: Text('إشحن',  style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
                            ),
                          ),
                        )
                      ],
                    ))

      ])
    );
  }
}
