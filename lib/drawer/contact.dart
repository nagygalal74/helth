import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> {
  TextEditingController controller;
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:ListView(
        children: <Widget>[
          Padding(
            padding:EdgeInsets.only(top: 15.0,right: 5.0) ,
            child:IconButton(
              alignment:Alignment.topRight ,
              onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>BottomNav())) ,
              icon:Image(image: AssetImage('img/backblack_@2x.png'),
                  color:color,height: 30.0,width: 30.0),),),
         Padding(padding:EdgeInsets.only(top: 5.0,right: 20.0) ,child: Text('إتصل بنا',
           style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 30.0),textDirection: TextDirection.rtl,)),
          SizedBox(height: 20.0,),
          Directionality(
            textDirection: TextDirection.rtl,
            child:ListTile(
              title: Padding(padding:EdgeInsets.only(right: 25.0,top: 5.0) ,
             child:Text('شكوتك أو إقتراحك',
             style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w500),)),
              subtitle: Padding(padding:EdgeInsets.only(right: 20.0,top: 5.0) ,
                child:TextFormField(
                  controller: controller,
                    decoration:InputDecoration(
                      enabledBorder:UnderlineInputBorder(
                        borderSide:BorderSide(color:color),
                      ),
                      focusedBorder:UnderlineInputBorder(
                        borderSide:BorderSide(color:color),
                      ),
                    )),
              ),
            )
          ),
          GestureDetector(
            child: Container(
              height: 45.0,
              padding: EdgeInsets.only(top: 5.0),
              margin: EdgeInsets.only(top: 40.0,right: 80.0,left: 80.0),
             decoration:  BoxDecoration(
             color: color,
            borderRadius: BorderRadius.circular(20)
            ),
              child: Text('إرسال',
            style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w500),textAlign: TextAlign.center,),
            ),
          ),

          Container(
            margin: EdgeInsets.only(top: 70.0),
            child: Column(
              children: <Widget>[
                Text('أو بلإتصال على الأرقام التالية',
                    style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:18 )),
                Container(
                  margin: EdgeInsets.only(top: 5.0) ,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                     Padding(padding:EdgeInsets.only(right: 10.0),
                         child:Text('+96655905095',
                       style: TextStyle(color:Colors.grey[700],fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:17 ))),
                      Padding(padding:EdgeInsets.only(right: 10.0),
                       child:Text('-',
                         style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:17 ))),
                      Padding(padding:EdgeInsets.only(right: 10.0),
                          child:Text('+96655905095',
                              style: TextStyle(color:Colors.grey[700],fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:17 ))),
                    ],
                  ),
                ),
               SizedBox(height: 20,),
               Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(icon: Image(image: AssetImage('img/snapchaticoncontact.png')),onPressed: (){},),
                    IconButton(icon: Image(image: AssetImage('img/instagramiconcontact.png')),onPressed: (){},),
                    IconButton(icon: Image(image: AssetImage('img/twittericoncontact.png')),onPressed: (){},),
                    IconButton(icon: Image(image: AssetImage('img/facebookiconcontact.png')),onPressed: (){},),

                  ],
                ),
              ],
            ),
          )

        ],

      )
    );
  }
}
