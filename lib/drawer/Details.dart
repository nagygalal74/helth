import 'package:flutter/material.dart';
import 'package:healthyzone/drawer/join%20us.dart';
import 'package:healthyzone/drawer/sendrequest.dart';
class Details extends StatefulWidget {
  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  TextEditingController fullName;
  TextEditingController number;
  TextEditingController Email;
  TextEditingController identify;
  TextEditingController postion;
  TextEditingController trade_Num;
  TextEditingController license;
  TextEditingController desc;
  TextEditingController logo;
  TextEditingController fav;

  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      backgroundColor: Colors.white,
      body:ListView(
          children: <Widget>[
      Padding(
      padding:EdgeInsets.only(top: 15.0,right: 5.0) ,
      child:IconButton(
        alignment:Alignment.topRight ,
        onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Join_us())) ,
        icon:Image(image: AssetImage('img/backblack_@2x.png'),
            color:color,height: 30.0,width: 30.0),),),
            Padding(padding:EdgeInsets.only(top: 2.0,right: 15.0) ,child:
            Text('إنضم إلينا اليوم',
              style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 28.0),textAlign: TextAlign.end,)),
            Padding(padding:EdgeInsets.only(top: 2.0,right: 15.0,left: 5.0) ,child:
            Text('تفضل بإرسال بياناتك كي نتمكن من خدمتكم في إدارة خدمات الشركاء',
              style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:15.0 ),textAlign: TextAlign.end,)),
            Padding(padding:EdgeInsets.only(top: 15.0,right: 15.0) ,child:
            Text('المعلومات الشخصية',
              style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:18 ),textAlign: TextAlign.end,)),
            Column(
              children: <Widget>[
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                       controller:fullName,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'الإسم بالكامل',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                      keyboardType:TextInputType.number,
                       controller:number,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'رقم الجوال',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        controller:Email,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'البريد الإلكتروني',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        controller:identify,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'صفة مقدم الطلب',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.rtl,
                  child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                    child:TextFormField(
                        controller:postion,
                        decoration:InputDecoration(
                          hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                          hintText: 'منصب مقدم الطلب',
                          enabledBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                          focusedBorder:UnderlineInputBorder(
                            borderSide:BorderSide(color: color),
                          ),
                        )),
                  ),
                ),
              ],
            ),
                  //==========================معلومات المركز*********===================================================
            Padding(padding:EdgeInsets.only(top: 28.0,right: 25.0) ,child:
            Text('معلومات المركز',
              style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 18),textAlign: TextAlign.end,)),
            Column(
                children: <Widget>[
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                      child:TextFormField(
                         controller:trade_Num,
                          decoration:InputDecoration(
                            hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                            hintText: 'الإسم التجاري',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                          )),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                      child:TextFormField(
                          controller:license,
                          decoration:InputDecoration(
                            suffixIcon: IconButton(icon: Image(image: AssetImage('img/attatch.png')), onPressed:(){}),
                            hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                            hintText: 'ترخيص رسمي إن وجد',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                          )),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                      child:TextFormField(
                          controller:desc,
                          decoration:InputDecoration(
                            hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                            hintText: 'وصف النشاط',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                          )),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                      child:TextFormField(
                          controller:logo,
                          decoration:InputDecoration(
                            suffixIcon: IconButton(icon: Image(image: AssetImage('img/attatch.png')), onPressed:(){}),
                            hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                            hintText: 'الشعار',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                          )),
                    ),
                  ),
                  Directionality(
                    textDirection: TextDirection.rtl,
                    child:Padding(padding:EdgeInsets.only(left: 25.0,right: 25.0,top: 7.0) ,
                      child:TextFormField(
                          controller:fav,
                          decoration:InputDecoration(
                            suffixIcon: IconButton(icon: Image(image: AssetImage('img/downarrow.png')), onPressed:(){}),
                            hintStyle: TextStyle(color:color,fontFamily:'Cairo',fontWeight: FontWeight.w400,fontSize:15  ),
                            hintText: 'الفترة المفضلة للإتصال',
                            enabledBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                            focusedBorder:UnderlineInputBorder(
                              borderSide:BorderSide(color: color),
                            ),
                          )),
                    ),
                  ),
          ]),
          Container(
                height:MediaQuery.of(context).size.height*.16,
                width: 70,
                child:Stack(
                  children: <Widget>[
                    Positioned(
                        right: 0.0,
                        left:0.0,
                        bottom: 0.0,
                        child:InkWell(
                          onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>sendRequest())),
                          child: Container(
                            alignment: Alignment.center,
                            height: 55.0,
                            color:color,
                            child: Text('إرسال',  style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
                          ),
                        ),
                    )
                  ],
                ))
          ])
    );
  }
}
