import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/drawer/Details.dart';
import 'package:healthyzone/pages/Home.dart';
class Join_us extends StatefulWidget {
  @override
  _Join_usState createState() => _Join_usState();
}

class _Join_usState extends State<Join_us> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection:TextDirection.rtl,
        child:Scaffold(
          backgroundColor: Colors.white,
          body:ListView(
              children: <Widget>[
                Padding(
                  padding:EdgeInsets.only(top: 15.0,right: 5.0) ,
                  child:IconButton(
                    alignment:Alignment.topRight ,
                    onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>BottomNav())) ,
                    icon:Image(image: AssetImage('img/backblack_@2x.png'),
                        color:color,height: 30.0,width: 30.0),),),
                Padding(padding:EdgeInsets.only(top: 5.0,right: 20.0) ,child:
                Text('الإنضمام معنا كشريك',
                  style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 28.0),)),
                Padding(padding: EdgeInsets.only(top: 20.0,right: 15.0,left: 15.0),
                    child:Text(
                        'إن كان لديك منشأة تجاريه متخصصة في تقديم الأنشطة الرياضية فانت في مكانك الصحيح للوصول الي عملاء جدد وزيادة مبيعاتك وتوسيع نشاطك بشكل كبير في زمن قياسيه  '

                            'تحفيز المجتمع لممارسة الرياضه عبر توفير الندية الرياضيه '
                            'في منصتنا بإشتراك يومي وباسعار متفاوته وجعلنا من منصتنا وسيلة مميزة وسهلة الإستخدام للبحث عن الأندية الرياضية في الموقع الذي يحدده العميل ويتم حجزها بشكل فوري '
                            'ولتحقيق هذا الهدف قمنا بتطوير نظام الكتروني مميز لشركائنا من مزودي الخدمات الرياضية لإضافة وغدارة خدماتهم الرياضية وبالإضافة الي خطة تسويقية ليمكنكم من الوصول الي شريحة اكبر من العملاء بسرعة وسهولة',
                      style: TextStyle(color: Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w500),)),
                SizedBox(height:150.0 ,),
                Container(
                    height:MediaQuery.of(context).size.height*.16,
                    width: 70,
                    child:Stack(
                      children: <Widget>[
                        Positioned(
                          right: 0.0,
                            left:0.0,
                            bottom: 0.0,
                            child:InkWell(
                              onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Details())),
                              child: Container(
                                alignment: Alignment.center,
                                height: 55.0,
                                color:color,
                                child: Text('إنضم الأن',  style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800),),
                              ),
                            )
                        )
                      ],
                    ))
              ]),),
    );
  }
}
