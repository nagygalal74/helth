import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
class Conditions extends StatefulWidget {
  @override
  _ConditionsState createState() => _ConditionsState();
}

class _ConditionsState extends State<Conditions> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection:TextDirection.rtl,
        child:Scaffold(
          backgroundColor: Colors.white,
          body:ListView(
              children: <Widget>[
                Padding(
                  padding:EdgeInsets.only(top: 15.0,right: 5.0) ,
                  child:IconButton(
                    alignment:Alignment.topRight ,
                    onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>BottomNav())) ,
                    icon:Image(image: AssetImage('img/backblack_@2x.png'),
                        color:color,height: 30.0,width: 30.0),),),
                Padding(padding:EdgeInsets.only(top: 5.0,right: 20.0) ,
                    child: Text('الشروط والأحكام',
                      style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 30.0),)),
                Padding(padding: EdgeInsets.only(top: 20.0,right: 15.0,left: 15.0),
                    child: Text('هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.'
                        'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق. إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.',
                      style: TextStyle(color: Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w500),)),
              ]),)
    );
  }
}
