import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/pages/Home.dart';
class sendRequest extends StatefulWidget {
  @override
  _sendRequestState createState() => _sendRequestState();
}

class _sendRequestState extends State<sendRequest> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:ListView(
        children: <Widget>[
        Center(
          child: Column(
        children: <Widget>[
          SizedBox(height: 200.0,),
          Image(image: AssetImage('img/header.png'),height: 70.0,),
         Padding(padding:EdgeInsets.only(top: 15.0) ,child: Text('تم إرسال طلبك',
           style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 24.0),)),
          Padding(padding:EdgeInsets.only(top: 15.0) ,child: Text('سيتم التواصل معك من خلال الإدارة',
            style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize: 18.0),)),
        ],
      )),
          SizedBox(height: 120,),
          Container(
              height:MediaQuery.of(context).size.height*.24,
              width: 70,
              child:Stack(
                children: <Widget>[
                  Positioned(
                    right: 0.0,
                    left:0.0,
                    bottom: 0.0,
                    child:InkWell(
                      onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context)=>BottomNav())),
                      child: Container(
                        alignment: Alignment.center,
                        height: 50.0,
                        color:color,
                        child: Text('الرئيسية',  style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
                      ),
                    ),
                  )
                ],
              ))
    ]),
    );
  }
}
