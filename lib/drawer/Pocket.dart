import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:healthyzone/BottomNavigation/Bottom%20Navigation.dart';
import 'package:healthyzone/drawer/charge.dart';
import 'package:healthyzone/pages/Home.dart';
class Pocket extends StatefulWidget {
  @override
  _PocketState createState() => _PocketState();
}

class _PocketState extends State<Pocket> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      backgroundColor: Colors.white,
      body:ListView(
          children: <Widget>[
      Padding(
      padding:EdgeInsets.only(top: 15.0,right: 5.0) ,
      child:IconButton(
        alignment:Alignment.topRight ,
        onPressed:()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>BottomNav())) ,
        icon:Image(image: AssetImage('img/backblack_@2x.png'),
            color:color,height: 30.0,width: 30.0),),),
    Padding(padding:EdgeInsets.only(top: 2.0,right: 15.0) ,child:
    Text('المحفظة',
    style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize: 28.0),textAlign: TextAlign.end,)),
    Padding(padding:EdgeInsets.only(top: 2.0,right: 15.0,left: 5.0) ,child:
    Text('قم بإضافة رصيد إلي محفظتك وإدفع من خلالها المبلغ لكل عملية إشتراك تقوم بها للمراكز',
    style: TextStyle(color:Colors.grey,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:15.0 ),textAlign: TextAlign.end,)),
        SizedBox(height: 90.0,),
        Center(
          child: Column(
            children: <Widget>[
              Text('رصيدك', style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:19.0 )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                 Padding(padding:EdgeInsets.only(top: 5.0,right: 9.0,left: 5.0) ,
                     child: Text('ريال',
                     style: TextStyle(color:Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:15.0 ))),
                  Text('0.0',
                      style: TextStyle(color:Colors.grey[600],fontFamily: 'Cairo',fontWeight: FontWeight.w800,fontSize:40.0 ),),
                ],
              ),
            ],
        )),
            SizedBox(height:MediaQuery.of(context).size.height*.26,),
            Container(
                height:MediaQuery.of(context).size.height*.16,
                width: 70,
                child:Stack(
                  children: <Widget>[
                    Positioned(
                      right:MediaQuery.of(context).size.height*0.0,
                      left:MediaQuery.of(context).size.height*0.0,
                      bottom: MediaQuery.of(context).size.height*0.0,
                      child:InkWell(
                        onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Charge_blance())),
                        child: Container(
                          alignment: Alignment.center,
                          height: 55.0,
                          color:color,
                          child: Text('شحن رصيد للمحفظة',  style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
                        ),
                      ),
                    )
                  ],
                ))

      ])
    );
  }
}
