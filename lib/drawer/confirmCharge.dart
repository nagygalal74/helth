import 'package:flutter/material.dart';

import 'Pocket.dart';
class confirmCharge extends StatefulWidget {
  @override
  _confirmChargeState createState() => _confirmChargeState();
}

class _confirmChargeState extends State<confirmCharge> {
  Color color=Color.fromARGB(242,12,82,123);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 70.0,),
         Container(
           margin: EdgeInsets.only(top: 20.0,right: 5.0),
           alignment: Alignment.topRight,
             child:Text('تم شحن المحفظة', style: TextStyle(color:color,fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:30.0 ))),
            Container(
                margin: EdgeInsets.only(top: 0.0,right: 25.0),
                alignment: Alignment.topRight,
                child:Text('ورصيدك اصبح', style: TextStyle(color:Colors.black,fontFamily: 'Cairo',fontWeight: FontWeight.w700,))),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(padding:EdgeInsets.only(top: 15.0,right: 9.0,left: 5.0) ,
                child: Text('ريال',
                    style: TextStyle(color:Colors.lightGreen,fontFamily: 'Cairo',fontWeight: FontWeight.w500,fontSize:15.0 ))),
            Text('99.00',
              style: TextStyle(color:Colors.grey[600],fontFamily: 'Cairo',fontWeight: FontWeight.w700,fontSize:40.0 ),),
            ]),
            SizedBox(height: MediaQuery.of(context).size.height*.46,),
            Container(
                height:MediaQuery.of(context).size.height*.19,
                width:MediaQuery.of(context).size.width*1,
                child:Stack(
                  children: <Widget>[
                    Positioned(
                      right:MediaQuery.of(context).size.height*0.0,
                      left:MediaQuery.of(context).size.height*0.0,
                      bottom: MediaQuery.of(context).size.height*0.0,
                      child:InkWell(
                        onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>Pocket())),
                        child: Container(
                          alignment: Alignment.center,
                          height: 55.0,
                          color:color,
                          child: Text('إستمرار',  style: TextStyle(color:Colors.white,fontFamily: 'Cairo',fontWeight: FontWeight.w800,),),
                        ),
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
