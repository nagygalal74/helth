import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:healthyzone/BottomNavigation/EditHealthy.dart';
import 'package:healthyzone/BottomNavigation/Favourit.dart';
import 'package:healthyzone/BottomNavigation/Favourit2.dart';
import 'package:healthyzone/BottomNavigation/MyData.dart';
import 'package:healthyzone/BottomNavigation/Naotification2.dart';
import 'package:healthyzone/BottomNavigation/Notification.dart';
import 'package:healthyzone/BottomNavigation/PersonalData.dart';
import 'package:healthyzone/BottomNavigation/Requests.dart';
import 'package:healthyzone/SplashScreen.dart';
import 'package:healthyzone/drawer/Pocket.dart';
import 'package:healthyzone/drawer/charge.dart';
import 'package:healthyzone/pages/Home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
//import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
import 'package:nepali_date_picker/nepali_date_picker.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:splashscreen/splashscreen.dart';
class Example extends StatefulWidget {
  @override
  ExampleState createState() => new ExampleState();
}




class ExampleState extends State<Example> {
  int currentTab = 0; // Index of currently opened tab.
  Home pageOne = new Home(); // Page that corresponds with the first tab.
  Requests pageTwo = new Requests(); // Page that corresponds with the second tab.
  Favouritpage pageThree = new Favouritpage(); // Page that corresponds with the third tab.
  List<Widget> pages; // List of all pages that can be opened from our BottomNavigationBar.

  // Index 0 represents the page for the 0th tab, index 1 represents the page for the 1st tab etc...

  Widget currentPage; // Page that is open at the moment.

  @override
  void initState() {
    super.initState();
    pages = [Home(), Requests(), Favourit()]; // Populate our pages list.
    currentPage = Home(); // Setting the first page that we'd like to show our user.

    // Notice that pageOne is the 0th item in the pages list. This corresponds with our initial currentTab value.

    // These two should match at the start of our application.
  }

  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    // Here we create our BottomNavigationBar.
    final BottomNavigationBar navBar = new BottomNavigationBar(
      currentIndex: currentTab, // Our currentIndex will be the currentTab value. So we need to update this whenever we tab on a new page!
      onTap: (int numTab) { // numTab will be the index of the tab that is pressed.
        setState(() { // Setting the state so we can show our new page.
          print("Current tab: " + numTab.toString()); // Printing for debugging reasons.
          currentTab = numTab; // Updating our currentTab with the tab that is pressed [See 43].
          currentPage = pages[numTab]; // Updating the page that we'd like to show to the user.
        });
      },
      items: <BottomNavigationBarItem>[ // Visuals, see docs for more information: https://docs.flutter.io/flutter/material/BottomNavigationBar-class.html
        new BottomNavigationBarItem( //numTab 0
            icon: new Icon(Icons.ac_unit),
            title: new Text("Ac unit")
        ),
        new BottomNavigationBarItem( //numTab 1
            icon: new Icon(Icons.access_alarm),
            title: new Text("Access alarm")
        ),
        new BottomNavigationBarItem( //numTab 2
            icon: new Icon(Icons.access_alarms),
            title: new Text("Access alarms")
        )
      ],

    );

    return new Scaffold(
      bottomNavigationBar: navBar, // Assigning our navBar to the Scaffold's bottomNavigationBar property.
      body: currentPage, // The body will be the currentPage. Which we update when a tab is pressed.
    );
  }
}

class PageOne extends StatelessWidget { // Creating a simple example page.
  @override
  Widget build(BuildContext context) {
    return new Center(child: new Text("Page one"));
  }
}

class PageTwo extends StatelessWidget { // Creating a simple example page.
  @override
  Widget build(BuildContext context) {
    return new Center(child: new Text("Page two"));
  }
}

class PageThree extends StatelessWidget { // Creating a simple example page.
  @override
  Widget build(BuildContext context) {
    return new Center(child: new Text("Page three"));
  }
}
/*class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  Color color=Color.fromARGB(242,12,82,123);
  var _currentIndex=0;
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;

    });
  }
  List<Widget>  pages=[
    Home(),
    Requests(),
    notification(),
    NotificationPage(),
    Favourit(),
    //Favouritpage(),
    PersonalData(),
    //MyData(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar:  Directionality(
          textDirection: TextDirection.rtl,
          child: BottomNavigationBar(
            onTap: onTabTapped,
            currentIndex: _currentIndex,
            backgroundColor: color,
            elevation: 0.0,
            unselectedItemColor: Colors.white30,
            selectedItemColor: Colors.lightGreen,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                  backgroundColor:color,
                  icon:Image(image: AssetImage('img/hometaplight@2x.png',),width: 30.0,),
                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('img/calendertap.png',),),
                  title: Text('')
              ),
            BottomNavigationBarItem(
              icon: Image(image: AssetImage('img/notificationstap.png',),),

              title: Text('')
          ),

              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('img/favouritetap.png',),),
                  title: Text('')
              ),
              BottomNavigationBarItem(
                  icon: Image(image: AssetImage('img/profiletap.png',),),
                  title: Text('')
              ),
            ],
          )),

      body:pages[_currentIndex]
    ) ;
  }
}*/

/*class VisibilityExample extends StatefulWidget {
  @override
  _VisibilityExampleState createState() {
    return _VisibilityExampleState();
  }
}

class _VisibilityExampleState extends State {
  bool _isVisible = true;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }
  void _showScaffold(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
    ));
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Visibility Tutorial by Woolha.com',
      home: Scaffold(
        key:_scaffoldKey,
          appBar: AppBar(
            title: Text('Visibility Tutorial by Woolha.com'),
          ),
          body: Padding(
            padding: EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RaisedButton(
                  child: Text('Show/Hide Card B'),
                  onPressed: showToast,
                ),
                Card(
                  child: new ListTile(
                    title: Center(
                      child: new Text('A'),
                    ),
                  ),
                ),
                Visibility (
                    visible: _isVisible,
                    child: Card(
                      child: new ListTile(
                        title: Center(
                          child: new Text('B'),
                        ),
                      ),
                    ),
                    replacement: Container(
                      margin: EdgeInsets.only(
                        top: 100.0
                      ),
                      child:   RaisedButton(
                                textColor: Colors.white,
                                color: Colors.green,
                                child: Text('تراجع عن الحذف'),
                                onPressed: showToast,
                              ),
                          ),
                    )
                ,


              ],
            ),
          ),
      ),
    );
  }


}*/
/*class TestApp extends StatefulWidget {
  @override
  _TestAppState createState() => _TestAppState();
}

class _TestAppState extends State<TestApp> {
  bool a = true;
  String mText = "Press to hide";
  Widget image=Image.asset('img/checkedbox.png');

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Visibility",
      home: new Scaffold(
          body: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new RaisedButton(
                onPressed: _visibilitymethod, child: new Text(mText),),
              a == true ? new Container(
                width: 300.0,
                height: 300.0,
                child: image,
              ) : new Container(
                color: Colors.black54,
              ),
            ],
          )
      ),
    );
  }

  void _visibilitymethod() {
    setState(() {
      if (a) {
        a = false;
        mText = "Press to show";
      } else {
        a = true;
        image;
      }
    });
  }
}*/

/*final page = new PageViewModel(
  pageColor: const Color(0xFF607D8B),
  iconImageAssetPath: 'assets/taxi-driver.png',
  iconColor: null,
  bubbleBackgroundColor: null,
  body: Text(
    'Easy  cab  booking  at  your  doorstep  with  cashless  payment  system',
  ),
  title: Text('Cabs'),
  mainImage: Image.asset(
    'assets/taxi.png',
    height: 285.0,
    width: 285.0,
    alignment: Alignment.center,
  ),
  titleTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
  bodyTextStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
);
class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child:  IntroViewsFlutter(
        [page],
        onTapDoneButton: (){
          //Void Callback
        },
        showSkipButton: true,
        pageButtonTextStyles: new TextStyle(
          color: Colors.white,
          fontSize: 18.0,
          fontFamily: "Regular",
        ),

      ),

    );
  }
}*/

/*class Category {
  const Category({ this.name, this.icon });
  final String name;
  final IconData icon;
}

// List of Category Data objects.
const List<Category> categories = <Category>[
  Category(name: 'General', icon: Icons.assessment),
  Category(name: 'Tech', icon: Icons.code),
  Category(name: 'Lifestyle', icon: Icons.people),
  Category(name: 'Finance', icon: Icons.account_balance),
  Category(name: 'Education', icon: Icons.school),
  Category(name: 'Sports', icon: Icons.settings_input_composite),

];

// Our MrTabs class.
//Will build and return our app structure.
class MrTabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new DefaultTabController(
        length: categories.length,
        child: new Scaffold(
          appBar: new AppBar(
            title: const Text('Tabbed AppBar'),
            bottom: new TabBar(
              isScrollable: true,
              tabs: categories.map((Category choice) {
                return new Tab(
                  text: choice.name,
                  icon: new Icon(choice.icon),
                );
              }).toList(),
            ),
          ),
          body: new TabBarView(
            children: categories.map((Category choice) {
              return new Padding(
                padding: const EdgeInsets.all(16.0),
                child: new CategoryCard(choice: choice),
              );
            }).toList(),
          ),
        ),
      ),
      theme: new ThemeData(primaryColor: Colors.deepOrange),
    );
  }
}

// Our CategoryCard data object
class CategoryCard extends StatelessWidget {
  const CategoryCard({ Key key, this.choice }) : super(key: key);
  final Category choice;

  //build and return our card with icon and text
  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return new Card(
      color: Colors.white,
      child: new Center(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Icon(choice.icon, size: 128.0, color: textStyle.color),
            new Text(choice.name, style: textStyle),
          ],
        ),
      ),
    );
  }
}*/
/*class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('jjjjj'),
      ),
      body:  new Swiper(
        itemBuilder: (BuildContext context,int index){
          return new Image.network("http://via.placeholder.com/350x150",fit: BoxFit.fill,);
        },
        itemCount: 3,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),


      ),
    );
  }
}*/


/*class ViewWidget extends StatefulWidget {
  @override
  ViewWidgetState createState() => ViewWidgetState();
}

class ViewWidgetState extends State {

  bool viewVisible = true ;

  void showWidget(){
    setState(() {
      viewVisible = true ;
    });
  }

  void hideWidget(){
    setState(() {
      viewVisible = false ;
    });
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[

        Visibility(
            maintainSize: true,
            maintainAnimation: true,
            maintainState: true,
            visible: viewVisible,
            child: Container(
                height: 200,
                width: 200,
                color: Colors.green,
                margin: EdgeInsets.only(top: 50, bottom: 30),
                child: Center(child: Text('Show Hide Text View Widget in Flutter',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white,
                        fontSize: 23)))
            )
        ),

        RaisedButton(
          child: Text('Hide Widget on Button Click'),
          onPressed: hideWidget,
          color: Colors.pink,
          textColor: Colors.white,
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        ),

        RaisedButton(
          child: Text('Show Widget on Button Click'),
          onPressed: showWidget,
          color: Colors.pink,
          textColor: Colors.white,
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
        ),

      ],
    );
  }
}*/


/*class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Container(
        child:  Slidable(
          slideToDismissDelegate: new SlideToDismissDrawerDelegate(
            onDismissed: (actionType) {
              _showSnackBar(
                  context,
                  actionType == SlideActionType.primary
                      ? 'Dismiss Archive'
                      : 'Dimiss Delete');
              setState(() {
                items.removeAt(index);
              });
            },
          ),
          delegate: new SlidableDrawerDelegate(),
          actionExtentRatio: 0.25,
          child: new Container(
            color: Colors.white,
            child: new ListTile(
              leading: new CircleAvatar(
                backgroundColor: Colors.indigoAccent,
                //child: new Text('$3'),
                foregroundColor: Colors.white,
              ),
             // title: new Text('Tile n°$3'),
              subtitle: new Text('SlidableDrawerDelegate'),
            ),
          ),
          actions: <Widget>[
            new IconSlideAction(
              caption: 'Archive',
              color: Colors.blue,
              icon: Icons.archive,
              //onTap: () => _showSnackBar('Archive'),
            ),
            new IconSlideAction(
              caption: 'Share',
              color: Colors.indigo,
              icon: Icons.share,
             // onTap: () => _showSnackBar('Share'),
            ),
          ],
          secondaryActions: <Widget>[
            new IconSlideAction(
              caption: 'More',
              color: Colors.black45,
              icon: Icons.more_horiz,
             // onTap: () => _showSnackBar('More'),
            ),
            new IconSlideAction(
              caption: 'Delete',
              color: Colors.red,
              icon: Icons.delete,
              //onTap: () => _showSnackBar('Delete'),
            ),
          ],
        ),
      )
    );
  }
}*/

/*class MyApp extends StatefulWidget {
  createState() => MyAppState();
}

List<String> listItems = [
  "Hello",
  "World",
  "Flutter",
  "Love"
]; //dummy list of items

class MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text("Swipe List"),
          ),
          body: Container(
              child: ListView.builder(
                itemCount: listItems.length,
                itemBuilder: (context, index) {
                  return Dismissible(

                    background: stackBehindDismiss(),
                    //crossAxisEndOffset: 3,
                    key: ObjectKey(listItems[index]),
                    crossAxisEndOffset: 0,
                    child: Container(
                      padding: EdgeInsets.all(20.0),
                      child: Text(listItems[index]),

                    ),
                    onDismissed: (direction) {
                      var item = listItems.elementAt(index);
                      //To delete
                      deleteItem(index);
                      //To show a snackbar with the UNDO button
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: Text("Item deleted"),
                          action: SnackBarAction(
                              label: "UNDO",
                              onPressed: () {
                                //To undo deletion
                                undoDeletion(index, item);
                              })));
                    },
                  );
                },
              )),
        ));
  }

  void deleteItem(index) {
    /*
    By implementing this method, it ensures that upon being dismissed from our widget tree,
    the item is removed from our list of items and our list is updated, hence
    preventing the "Dismissed widget still in widget tree error" when we reload.
    */
    setState(() {
      listItems.removeAt(index);
    });
  }

  void undoDeletion(index, item) {
    /*
    This method accepts the parameters index and item and re-inserts the {item} at
    index {index}
    */
    setState(() {
      listItems.insert(index, item);
    });
  }

  Widget stackBehindDismiss() {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.only(right: 20.0),
      color: Colors.red,
      child: Icon(
        Icons.delete,
        color: Colors.white,
      ),
    );
  }
}*/

/*class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  bool visibilityTag = false;
  bool visibilityObs = false;

  void _changed(bool visibility, String field) {
    setState(() {
      if (field == "tag"){
        visibilityTag = visibility;
      }
      if (field == "obs"){
        visibilityObs =visibility;
      }
    });
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
        appBar: new AppBar(backgroundColor: new Color(0xFF26C6DA)),
        body: new ListView(
          children: <Widget>[
            new Container(
              margin: new EdgeInsets.all(20.0),
              child: new FlutterLogo(size: 100.0, colors: Colors.blue),
            ),
            new Container(
                margin: new EdgeInsets.only(left: 16.0, right: 16.0),
                child: new Column(
                  children: <Widget>[
                    visibilityObs ? new Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Expanded(
                          flex: 11,
                          child: new TextField(
                            maxLines: 1,
                            style: Theme.of(context).textTheme.title,
                            decoration: new InputDecoration(
                                labelText: "Observation",
                                isDense: true
                            ),
                          ),
                        ),
                        new Expanded(
                          flex: 1,
                          child: new IconButton(
                            color: Colors.grey[400],
                            icon: const Icon(Icons.cancel, size: 22.0,),
                            onPressed: () {
                              _changed(false, "obs");
                            },
                          ),
                        ),
                      ],
                    ) : new Container(),

                    visibilityTag ? new Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new Expanded(
                          flex: 11,
                          child: new TextField(
                            maxLines: 1,
                            style: Theme.of(context).textTheme.title,
                            decoration: new InputDecoration(
                                labelText: "Tags",
                                isDense: true
                            ),
                          ),
                        ),
                        new Expanded(
                          flex: 1,
                          child: new IconButton(
                            color: Colors.grey[400],
                            icon: const Icon(Icons.cancel, size: 22.0,),
                            onPressed: () {
                              _changed(false, "tag");
                            },
                          ),
                        ),
                      ],
                    ) : new Text('kkkkk'),
                  ],
                )
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new InkWell(
                    onTap: () {
                      visibilityObs ? null : _changed(true, "obs");
                    },
                    child: new Container(
                      margin: new EdgeInsets.only(top: 16.0),
                      child: new Column(
                        children: <Widget>[
                          new Icon(Icons.comment, color: visibilityObs ? Colors.grey[400] : Colors.grey[600]),
                          new Container(
                            margin: const EdgeInsets.only(top: 8.0),
                            child: new Text(
                              "Observation",
                              style: new TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.w400,
                                color: visibilityObs ? Colors.grey[400] : Colors.grey[600],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                ),
                new SizedBox(width: 24.0),
                new InkWell(
                    onTap: () {
                      visibilityTag ? null : _changed(true, "tag");
                    },
                    child: new Container(
                      margin: new EdgeInsets.only(top: 16.0),
                      child: new Column(
                        children: <Widget>[
                          new Icon(Icons.local_offer, color: visibilityTag ? Colors.grey[400] : Colors.grey[600]),
                          new Container(
                            margin: const EdgeInsets.only(top: 8.0),
                            child: new Text(
                              "Tags",
                              style: new TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.w400,
                                color: visibilityTag ? Colors.grey[400] : Colors.grey[600],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                ),
              ],
            )
          ],
        )
    );
  }
}*/

/*class RadioGroup extends StatefulWidget {
  @override
  _RadioGroupState createState() => _RadioGroupState();
}

class _RadioGroupState extends State<RadioGroup> {
  int _currentIndex = 1;

  List<GroupModel> _Healthy = [
    GroupModel(
      text: "نعم",
      index: 1,
    ),
    GroupModel(
      text: "لا",
      index: 2,
    ),

  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("RadioGroup Example"),
      ),
      body: ListView(
        padding: EdgeInsets.all(8.0),
        children: _Healthy
            .map((item) => RadioListTile(
          groupValue: _currentIndex,
          title: Text("${item.text}"),
          value: item.index,
          onChanged: (val) {
            setState(() {
              _currentIndex = val;
            });
          },
        ))
            .toList(),
      ),
    );
  }
}

class GroupModel {
  String text;
  int index;
  GroupModel({this.text, this.index});
}*/

/*class MyApp extends StatefulWidget {
  @override
  _State createState() => new _State();
}
class   Healthy {
  String name;
  int index;
  Healthy({this.name, this.index});
}

//State is information of the application that can change over time or when some actions are taken.
class _State extends State<MyApp>{
  int id = 1;

  List<Healthy> fList = [
    Healthy(
      index: 1,
      name: "نعم",
    ),
    Healthy(
      index: 2,
      name: "لا",
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[


    Expanded(
    child: Container(
    height: 200.0,
    child: Column(
    children:
    fList.map((data) => RadioListTile(
    groupValue: id,
    value: data.index,
    onChanged: (val) {
    setState(() {
    //radioItem = data.name ;
    id = data.index;
    });
    },
    )).toList(),
    ),
    )),

    ],
    ));
  }
}*/





/*class PickerThemeDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: const Text('Picker theme demo')),
      body: new Container(),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.date_range),
        onPressed: () => showDatePicker(
          context: context,
          initialDate: new DateTime.now(),
          firstDate: new DateTime.now().subtract(new Duration(days: 30)),
          lastDate: new DateTime.now().add(new Duration(days: 30)),
        ),
      ),
    );
  }
}

Color hexToColor(int rgb) => new Color(0xFF000000 + rgb);

class CustomTheme extends Theme {
  //Primary Blue: #335C81 (51, 92, 129)
  //Light Blue:   #74B3CE (116, 179, 206)
  //Yellow:       #FCA311 (252, 163, 17)
  //Red:          #E15554 (255, 85, 84)
  //Green:        #3BB273 (59, 178, 115)

  static Color blueDark = hexToColor(0x335C81);
  static Color blueLight = hexToColor(0x74B3CE);
  static Color yellow = hexToColor(0xFCA311);
  static Color red = hexToColor(0xE15554);
  static Color green = hexToColor(0x3BB273);

  CustomTheme(Widget child)
      : super(
    child: child,
    data: new ThemeData(
      primaryColor: blueDark,
      accentColor: yellow,
      cardColor: blueLight,
      backgroundColor: blueDark,
      highlightColor: red,
      splashColor: green,
    ),
  );
}
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ,
    );
  }
}*/



//import 'package:file_picker/file_picker.dart';
//import 'package:simple_share/simple_share.dart';
/*class HomePage extends StatefulWidget {
  _HomePageDemoState createState() => _HomePageDemoState();
}

class _HomePageDemoState extends State<HomePage> {
  List<String> taskList = [
    'The Enchanted Nightingale',
    'The Wizard of Oz',
    'The BFG'
  ];
  bool isPriority = false;

  void _toggleFlag() {
    setState(() {
      if (isPriority) {
        isPriority = false;
      } else {
        isPriority = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                final item = taskList[index];
                return Card(
                  elevation: 8.0,
                  child: ListTile(
                    contentPadding: EdgeInsets.all(0.0),
                    leading: Container(
                      color: (isPriority) ? Colors.red : Colors.grey,
                      //padding: EdgeInsets.only(right: 12.0),
                      padding: EdgeInsets.all(20.0),
                      child: Text(
                        '01',
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                    title: Text('$item'), //Text('The Enchanted Nightingale'),
                    subtitle:
                    Text('Music by Julie Gable. Lyrics by Sidney Stein.'),


                    trailing: IconButton(
                      key: Key(item),
                      icon: Icon(Icons.flag),
                      color: (isPriority) ? Colors.red : Colors.grey,
                      onPressed: _toggleFlag,
                    ),
                  ),
                );
              },
              childCount: taskList.length,
            ),
          ),
        ],
      ),
    );
  }
}*/

/*const Color _colorOne = Color(0x33000000);
const Color _colorTwo = Color(0x24000000);
const Color _colorThree = Color(0x1F000000);

void main() {
  runApp(new CupertinoSegmentedControlDemo());
}

class CupertinoSegmentedControlDemo extends StatefulWidget {

  @override
  _CupertinoSegmentedControlDemoState createState() =>
      _CupertinoSegmentedControlDemoState();
}

class _CupertinoSegmentedControlDemoState
    extends State<CupertinoSegmentedControlDemo> {
  final Map<int, Widget> logoWidgets = const <int, Widget>{
    0: Text('Logo 1'),
    1: Text('Logo 2'),
    2: Text('Logo 3'),
  };

  final Map<int, Widget> icons = const <int, Widget>{
    0: Center(
      child: FlutterLogo(
        colors: Colors.indigo,
        size: 200.0,
      ),
    ),
    1: Center(
      child: FlutterLogo(
        colors: Colors.teal,
        size: 200.0,
      ),
    ),
    2: Center(
      child: FlutterLogo(
        colors: Colors.cyan,
        size: 200.0,
      ),
    ),
  };

  int sharedValue = 0;

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: AppBar(
          title: const Text('Flutter SegmentedControl'),
        ),
        body: Column(
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.all(16.0),
            ),
            SizedBox(
              width: 500.0,
              child: CupertinoSegmentedControl<int>(
                children: logoWidgets,
                onValueChanged: (int val) {
                  setState(() {
                    sharedValue = val;
                  });
                },
                groupValue: sharedValue,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 32.0,
                  horizontal: 16.0,
                ),
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 64.0,
                    horizontal: 16.0,
                  ),
                  decoration: BoxDecoration(
                    color: CupertinoColors.white,
                    borderRadius: BorderRadius.circular(3.0),
                    boxShadow: const <BoxShadow>[
                      BoxShadow(
                        offset: Offset(0.0, 3.0),
                        blurRadius: 5.0,
                        spreadRadius: -1.0,
                        color: _colorOne,
                      ),
                      BoxShadow(
                        offset: Offset(0.0, 6.0),
                        blurRadius: 10.0,
                        spreadRadius: 0.0,
                        color: _colorTwo,
                      ),
                      BoxShadow(
                        offset: Offset(0.0, 1.0),
                        blurRadius: 18.0,
                        spreadRadius: 0.0,
                        color: _colorThree,
                      ),
                    ],
                  ),
                  child: icons[sharedValue],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}*/

/*class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'msc',
      home: new DefaultTabController(
        length: 2,
        child: new Scaffold(
          appBar: new PreferredSize(
            preferredSize: Size.fromHeight(kTextTabBarHeight),
            child: new Container(
              height: 300.0,
              color: Colors.green,
              child: new SafeArea(
                child: Column(
                  children: <Widget>[
                    new Expanded(child: new Container(height: 100.0,)),
                    new TabBar(
                      tabs: [new Text("Lunches"), new Text("Cart")],
                    ),
                  ],
                ),
              ),
            ),
          ),
          body: new TabBarView(
            children: <Widget>[
              new Column(
                children: <Widget>[new Text("Lunches Page")],
              ),
              new Column(
                children: <Widget>[new Text("Cart Page")],
              )
            ],
          ),
        ),
      ),
    );

  }
}*/

