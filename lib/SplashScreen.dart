import 'package:flutter/material.dart';
import 'package:healthyzone/pages/Intro_Screen.dart';
import 'package:splashscreen/splashscreen.dart';
class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:Color.fromARGB(242,12,82,123),
      body:Container(
        alignment: Alignment.center,
        child: Stack(
          children: <Widget>[
            SplashScreen(
              seconds:5,
              backgroundColor: Color.fromARGB(242,12,82,123),
              loaderColor: Colors.transparent,
              image: Image.asset('img/logo@2x.png'),
              photoSize: 45,
              navigateAfterSeconds: IntroPage(),
            ),
            Positioned(
              bottom: 50.0,
                right: 50.0,
                left: 50.0,
                child:Center(child:Image.asset('img/hz.png')),
            )
          ],
        ),
      )



    );
  }
}
